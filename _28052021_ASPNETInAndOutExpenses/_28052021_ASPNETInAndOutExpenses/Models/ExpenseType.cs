﻿using System.ComponentModel.DataAnnotations;

namespace _28052021_ASPNETInAndOutExpenses.Models
{
    public class ExpenseType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}