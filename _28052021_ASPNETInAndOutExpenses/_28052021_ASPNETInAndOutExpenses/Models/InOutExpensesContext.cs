using Microsoft.EntityFrameworkCore;
using _28052021_ASPNETInAndOutExpenses.Models;

namespace _28052021_ASPNETInAndOutExpenses.Models
{
    public class InOutExpensesContext:DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Expenses> Expenses { get; set; }
        public InOutExpensesContext()
        {
            
        }
        public InOutExpensesContext(DbContextOptions<InOutExpensesContext> options):
            base(options){}
        public DbSet<_28052021_ASPNETInAndOutExpenses.Models.ExpenseType> ExpenseType { get; set; }
    }
}