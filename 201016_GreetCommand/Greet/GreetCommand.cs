﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Greet
{
    class GreetCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        GreetVM vm;
        public GreetCommand(GreetVM v)
        {
            vm = v;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            MessageBox.Show("Hallo " + vm.Name);
        }
    }
}
