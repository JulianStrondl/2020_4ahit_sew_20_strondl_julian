﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Greet
{
    class GreetVM : INotifyPropertyChanged
    {
        public GreetVM()
        {
            g1 = new GreetCommand(this);
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void PropertyChange([CallerMemberName]string name=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        } 
        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                PropertyChange("Name");
            }
        }

        GreetCommand g1;
        public GreetCommand Greet1
        {
            get
            {
                return g1;
            }
        }
        public RelayCommand Greet2 => new RelayCommand(() => { MessageBox.Show("Hallo " + this.Name); });
        
    }
}
