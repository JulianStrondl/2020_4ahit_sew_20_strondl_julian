﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Greet
{
    class RelayCommand:ICommand
    {
        public event EventHandler CanExecuteChanged;
        readonly Predicate<object> _canExecute;
        readonly Action _execute;

        public RelayCommand(Action execute) : this(execute, null)
        {
            

        }
        public RelayCommand(Action execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute();
        }
    }
}
