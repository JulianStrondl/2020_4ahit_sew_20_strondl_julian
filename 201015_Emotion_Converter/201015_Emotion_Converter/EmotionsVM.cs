﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Windows.Media;

namespace _201015_Emotion_Converter
{
    public enum EMood { GRANTIG,SCHLECHT, GEHT_SO,BISSI_BESSA, LEIWOND}
    class EmotionsVM : INotifyPropertyChanged
    {
        public EmotionsVM()
        {
            mood = EMood.LEIWOND;
        }
        void OnPropertyChange([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public EMood Mood
        {
            get { return mood; }
            set
            {
                mood = value;
                OnPropertyChange("Mood");

            }
        }
        EMood mood;


    }
}
