﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace _201015_Emotion_Converter
{
    class EnumPictureConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is EMood && value != null)
            {

                EMood mood = (EMood)value;

                string c = "";
                switch (mood)
                {
                    case EMood.GRANTIG:
                        c = @"F:\Klassen\4AHIT\SEW\2020_4ahit_sew_20_strondl_julian\201015_Emotion_Converter\201015_Emotion_Converter\bin\Debug\PICTURES\VERY_BAD.jpg";
                        break;
                    case EMood.SCHLECHT:
                        c = @"F:\Klassen\4AHIT\SEW\2020_4ahit_sew_20_strondl_julian\201015_Emotion_Converter\201015_Emotion_Converter\bin\Debug\PICTURES\BAD.jpg";
                        break;
                    case EMood.GEHT_SO:
                        c = @"F:\Klassen\4AHIT\SEW\2020_4ahit_sew_20_strondl_julian\201015_Emotion_Converter\201015_Emotion_Converter\bin\Debug\PICTURES\OK.jpg";
                        break;
                    case EMood.BISSI_BESSA:
                        c = @"F:\Klassen\4AHIT\SEW\2020_4ahit_sew_20_strondl_julian\201015_Emotion_Converter\201015_Emotion_Converter\bin\Debug\PICTURES\GOOD.jpg";
                        break;
                    case EMood.LEIWOND:
                        c = @"F:\Klassen\4AHIT\SEW\2020_4ahit_sew_20_strondl_julian\201015_Emotion_Converter\201015_Emotion_Converter\bin\Debug\PICTURES\VERY_GOOD.jpg";
                        break;
                    default:
                        break;

                }
                //MessageBox.Show(c.ToString());
                return c;
            }
            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
