﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;
using System.Windows;

namespace _201015_Emotion_Converter
{
    class EnumColorConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is EMood && value !=null)
            {
                
                EMood mood = (EMood)value;
                
                SolidColorBrush c = new SolidColorBrush(Color.FromRgb(255,0,0));
                switch (mood)
                {
                    case EMood.GRANTIG:
                        c = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                        break;
                    case EMood.SCHLECHT:
                        c =  new SolidColorBrush(Color.FromRgb(255,165,0));
                        break;
                    case EMood.GEHT_SO:
                        c = new SolidColorBrush(Color.FromRgb(255, 255, 0));
                        break;
                    case EMood.BISSI_BESSA:
                        c = new SolidColorBrush(Color.FromRgb(192, 255, 62));
                        break;
                    case EMood.LEIWOND:
                        c = new SolidColorBrush(Color.FromRgb(0, 255, 0));
                        break;
                    default:
                        break;

                }
                //MessageBox.Show(c.ToString());
                return c;
            }
            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }
}
