﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class LinqExamplesTests
    {
        [TestMethod()]
        public void SelectScoresGreaterTest()
        {
            int[] scores = { 90, 71, 82, 93, 75, 82, 100, 39, 99 };
            int greaterThan = 80;

            //Test Score Funktions
            int[] resultM;
            int[] resultF; 
            int[] expectedResult = new int[] { 100, 99, 93, 90, 82, 82 };
            for (int i = 0; i < expectedResult.Length; i++)
            {
                Assert.Fail("Unit Test is missing");
            }
        }

        [TestMethod()]
        public void SortWordsTest()
        {
            string[] words = { "Affenbrotbaum", "Chiasamen",
                       "Acai", "Matcha", "Flohsamenschalen" };
            Console.WriteLine("\nSort Words Alphabetically");
            string[] names = Ex2_LinqQueryExamples.MSortWords(words);

            //Test your SortMethods
            string[] resultFLengthOrder; 
            string[] resultFOrder;  
            string[] resultMLengthOrder;
            string[] resultMOrder;
            string[] expectedLenghtOrder = { "Flohsamenschalen",
                "Affenbrotbaum", "Chiasamen", "Matcha", "Acai" };
            string[] expectedWordOrder = { "Acai", "Affenbrotbaum",
                "Chiasamen", "Flohsamenschalen", "Matcha" };

            for (int i = 0; i < expectedLenghtOrder.Length; i++)
            {
                Assert.Fail("Unit Test is missing");
            }

        }
   
        [TestMethod()]
        public void OrderByLenghtThenByAlphabetTest()
        {
            //write test case
            Assert.Fail("Unit Test is missing");
        }
    }
}
