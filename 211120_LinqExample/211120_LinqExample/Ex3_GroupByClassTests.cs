﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class GroupByClassTests
    {
      
        [TestMethod()]
        public void GroupingByFirstLetterDescendingTest()
        {

            //Change the TestMethod to a Unittest
            string[] foodList = { "carrots", "corn", "cucumber", "cabbage", "broccoli", "beans",
            "barley", "garlic","ginger", "onions", "olives", "orache", "orka", "radicchio",
            "parsnip", "pinto beans", "pumpkin" };

            IDictionary<char, int> result = Ex3_LinqGroupingExample.GroupByClass.GroupingByFirstLetterDescending(foodList);

            IDictionary<char, int> expected = new Dictionary<char, int>
                {
                    {'c',4},
                    {'o',4},
                    {'b',3},
                    {'p',3},
                    {'g',2},
                    {'r',1}
                };

            foreach (var item in expected.Keys)
            {
                Assert.AreEqual(result[item], expected[item]);
            }


            string[] names = { "Benedikt", "Raphael", "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };

            

            IDictionary<char, int> groupedFirstLetter = Ex3_LinqGroupingExample.GroupByClass.GroupingByFirstLetterDescending(names);

            IDictionary<char, int> expectedNames = new Dictionary<char, int>
                {
                    {'B',2},
                    {'R',1},
                    {'S',2},
                    {'A',2},
                };

            foreach (var item in expectedNames.Keys)
            {
                Assert.AreEqual(groupedFirstLetter[item], expectedNames[item]);
            }
        }
        }
    }
}