﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples
{
    public class Ex3_LinqGroupingExample
    {
        public static void TestLinqBsp()
        {
            PrintTitle("InnerJoin");
            InnerJoinClass.InnerJoinExample();
            PrintTitle("Left Outer Join");
            OuterJoinClass.LeftOuterJoinExample();
            PrintTitle("Groupby");
            GroupByClass.GroupByExampleList();
            PrintTitle("Groupby and Count");
            GroupByClass.GroupByExampleAmount();
            PrintTitle("Groupby and Sort");
            GroupByClass.TestGroupBy_ParaRetFood();
            GroupByClass.TestGroupby_ParaRetStudents();
        }

        public static void PrintTitle(String text)
        {
            Console.WriteLine("\n-------------------------------");
            Console.WriteLine("{0}", text);
            Console.WriteLine("-------------------------------");
        }

        #region Inner Join
        public class InnerJoinClass
        {
            class Book
            {
                public int BookID { get; set; }
                public string BookNm { get; set; }
            }

            class Order
            {
                public int OrderID { get; set; }
                public int BookID { get; set; }
                public string PaymentMode { get; set; }
            }

            public static void InnerJoinExample()
            {
                List<Book> bookList = new List<Book>
                {
                    new Book{BookID=1, BookNm="Java EE 7"},
                    new Book{BookID=2, BookNm="Pro JPA 2"},
                    new Book{BookID=3, BookNm="Microservices Pattern"},
                    new Book{BookID=4, BookNm="Design Patterns"},
                    new Book{BookID=5, BookNm="C# 6.0 and the .Net 4.6 Framework"}
                };

                List<Order> bookOrders = new List<Order>{
                    new Order{OrderID=1, BookID=1, PaymentMode="Cheque"},
                    new Order{OrderID=2, BookID=5, PaymentMode="Credit"},
                    new Order{OrderID=3, BookID=1, PaymentMode="Cash"},
                    new Order{OrderID=4, BookID=3, PaymentMode="Cheque"},
                    new Order{OrderID=5, BookID=3, PaymentMode="Cheque"},
                    new Order{OrderID=6, BookID=4, PaymentMode="Cash"}
                };

                //Gesucht ist eine Liste von allen Bestellungen mit Buchnummer, Buchname und Zahlungsmodus
                //Gib diese in der Konsole aus. 
                //Nutze die Freie Notation zum Erstellen der Abfrage.
                var orderForBooks = from bk in bookList
                                    join orders in bookOrders
                                    on bk.BookID equals orders.BookID
                                    select new
                                    {
                                        bk.BookID,
                                        Name = bk.BookNm,
                                        orders.PaymentMode
                                    };
                foreach (var b in orderForBooks)
                {
                    Console.WriteLine($"{b.BookID} {b.Name} {b.PaymentMode}");
                }
                /* Output
                1 - Java EE 7                           payed: Cheque
                1 - Java EE 7                           payed: Cash
                3 - Microservices Pattern               payed: Cheque
                3 - Microservices Pattern               payed: Cheque
                4 - Design Patterns                     payed: Cash
                5 - C# 6.0 and the .Net 4.6 Framework   payed: Credit
                */
            }
        }
        #endregion

        #region Outer Join
        public class OuterJoinClass
        {
            class Person
            {
                public string FirstName { get; set; }
                public string LastName { get; set; }
            }

            class Pet
            {
                public string Name { get; set; }
                public Person Owner { get; set; }
            }

            public static void LeftOuterJoinExample()
            {
                Person archie = new Person { FirstName = "Archie", LastName = "Andrews" };
                Person betty = new Person { FirstName = "Betty", LastName = "Cooper" };
                Person veronica = new Person { FirstName = "Veronica", LastName = "Lodge" };
                Person kevin = new Person { FirstName = "Kevin", LastName = "Keller" };

                Pet barley = new Pet { Name = "Barley", Owner = betty };
                Pet boots = new Pet { Name = "Boots", Owner = betty };
                Pet whiskers = new Pet { Name = "Whiskers", Owner = veronica };
                Pet bluemoon = new Pet { Name = "Blue Moon", Owner = betty };
                Pet daisy = new Pet { Name = "Daisy", Owner = archie };

                // Create two lists.
                List<Person> people = new List<Person> { archie, betty, veronica, kevin };
                List<Pet> pets = new List<Pet> { barley, boots, whiskers, bluemoon, daisy };


                //Selektiere alle Personen (Namen) 
                //liste dazu alle dazugehörigen Haustiere auf (falls vorhanden)
                //wenn nicht vorhanden verwende String.Empty
                //Nutze die Freie Notation zum Erstellen der Abfrage.
                var query = from pers in people
                            join pet in pets on pers equals pet.Owner into gj
                            from subp in gj.DefaultIfEmpty()
                            select new
                            {
                                pers.FirstName,
                                PetName = (subp == null ? "" : subp.Name)
                            };


                foreach(var q in query)
                {
                    Console.WriteLine($"{q.FirstName} {q.PetName}");
                }

                /* Output: 
                Archie:        Daisy
                Betty:         Barley
                Betty:         Boots
                Betty:         Blue Moon
                Veronica:      Whiskers
                Kevin:
                */
            }
        }
        #endregion

        #region Group By
        public class GroupByClass
        {
            static string[] foodList = { "carrots", "corn", "curcumber", "cabbage", "broccoli", "beans",
                "barley", "garlic","ginger", "onions", "olives", "orache", "orka", "radicchio",
                "parsnip", "pinto beans", "pumpkin" };

            public static void GroupByExampleList()
            {
                //Gruppiere nach Anfangsbuchstaben der Zeichenketten
                //Gib Anfangsbuchstabe und dann 
                //die Liste der dazugehörigen Gruppenelemente aus
                //Nutze die Freie Notation zum Erstellen der Abfrage.
                var queryFoodGroups =
                    from item in foodList
                    group item by item[0] into food
                    select new { Key = food.Key, Food = food };


                foreach (var item in queryFoodGroups)
                {
                    Console.WriteLine(item.Key +": ");
                    foreach (var  f in item.Food)
                    {
                        Console.Write("{0,10}",f);
                    }
                    Console.WriteLine();
                }

                /*
                c:     carrots       corn  curcumber    cabbage
                b:    broccoli      beans     barley
                g:      garlic     ginger
                o:      onions     olives     orache       orka
                r:   radicchio
                p:     parsnip pinto beans    pumpkin
                */
            }
            public static void GroupByExampleAmount()
            {
                //Gruppiere nach Anfangsbuchstaben der Zeichenketten - Zähle die Anzahl der Gruppenelemente
                //Gib Anfangsbuchstabe und dann die Anzahl der dazugehörigen Gruppenelemente aus

                var query = from item in foodList
                            group item by item[0] into food
                            select new { Key = food.Key, Count = food.Count() };

                foreach (var item in query)
                {
                    Console.WriteLine($"{item.Key} {item.Count}");
                }
                            
                /*   -------------------------------
                      Groupby and Count
                      -------------------------------
                      Buchstabe: c    Anzahl: 4
                      Buchstabe: b    Anzahl: 3
                      Buchstabe: g    Anzahl: 2
                      Buchstabe: o    Anzahl: 4
                      Buchstabe: r    Anzahl: 1
                      Buchstabe: p    Anzahl: 3
               */
            }

            #region Grouping and Sorting
            public static void TestGroupBy_ParaRetFood()
            {
                IDictionary<char, int> result =
                    GroupingByFirstLetterDescending(foodList);

                foreach (var item in result)
                {
                    Console.WriteLine("Buchstabe: {0}, Anzahl: {1}", item.Key, item.Value);
                }
            }
            public static void TestGroupby_ParaRetStudents()
            {
                string[] names = { "Benedikt", "Raphael",
            "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
                //Program.PrintList(names, "Grouping Names by First Letter");
                IDictionary<char, int> groupedFirstLetter =
                    GroupingByFirstLetterDescending(names);

                foreach (var letter in groupedFirstLetter)
                {
                    Console.WriteLine("{0}:{1}", letter.Key.ToString(), letter.Value);
                }
            }


            public static IDictionary<char, int> GroupingByFirstLetterDescending(string[] groupingQuery)
            {
                //group by first letter, 
                //sort result first by count descending then by key ascending

                var queryFoodGroups =
                    from item in groupingQuery
                    group item by item[0] into food
                    orderby food.Key ascending
                    orderby food.Count() descending
                    select new { Key = food.Key, Count = food.Count() };

                return queryFoodGroups.ToDictionary(f => f.Key, f => f.Count); 


                
            }
            #endregion


        }
        #endregion
    }

}



