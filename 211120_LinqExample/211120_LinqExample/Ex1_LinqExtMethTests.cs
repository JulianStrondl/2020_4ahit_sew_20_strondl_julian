﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class LinqExtensionMethodsTests
    {
        [TestMethod()]
        public void SortNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            //Test SortNames Method
            string[] sorted = LinqExtensionMethods.SortNames(names);
            for (int i = 0; i < sorted.Length - 1; i++)
            {
                Assert.IsTrue(sorted[i].CompareTo(sorted[i+1])< 0);
            }
            
        }

        [TestMethod()]
        public void ConcatNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            string[] pupils = NameGenerator.GetPupils();

            //Test ConcatNames Method
            string[] result= LinqExtensionMethods.ConcatNames(names, pupils);

            //Use the Length of the collections: names+pupils == result 
            Assert.IsTrue(names.Length + pupils.Length == result.Length);
        }

        [TestMethod()]
        public void RemoveRedundantTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());
            string[] concat = LinqExtensionMethods.ConcatNames(names,pupils);
            
            //Test remove redundant
            List<string> result = LinqExtensionMethods.RemoveRedundant(concat).ToList();

            //if you take out element by element - every one should be unique
            //so there should not be a second one with the same value in the list
            
            while (result.Count > 1)
            {
                string refa = result[0];
                result.RemoveAt(0);
                Assert.IsFalse(result.Contains(refa ));

            }
            
        }

        [TestMethod()]
        public void UnionNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());

            //Use Concat & Remove 
            string[] concat = LinqExtensionMethods.ConcatNames(names,pupils);
            string[] result = LinqExtensionMethods.RemoveRedundant(concat);

            //Test UnionNames 
            string[] union= LinqExtensionMethods.UnionNames(names,pupils);

            //Both collections should have the same amount of elements (result == union)
            Assert.AreEqual(result.Length,union.Length);
        }

        [TestMethod()]
        public void ReverseNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            //Test ReverseName Method
            string[] result = LinqExtensionMethods.ReverseNames(names);

            //First element should now be the last element
            Assert.IsTrue(names[names.Length-1]== result[0]);
        }
    }

    [TestClass()]
    public class LinqPetMethodsTests { 
        //TODO: Test your own PetMethods
        [TestMethod]
        public void ConcatPets() {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            IEnumerable<string> query = LinqExtensionMethods.PetConcaterClass.ConcatArrays(cats, dogs);
            
            //Überprüfung hinzufügen
            Assert.AreEqual(cats.Length + dogs.Length, query.ToList().Count);

            Console.WriteLine("Concat Pets into a collection of Names:");
            foreach (var e in query)
                Console.Write("{0}, ", e);
            Console.WriteLine("\n");
        }
        [TestMethod()]
        public void Averagepets()
        {
            Pet[] cats = Pet.GetCats();
            double avrgage = 0;
            for (int i = 0; i < cats.Length; i++)
            {
                avrgage += cats[i].Age;
            }
            avrgage = avrgage / cats.Length;
            
            Assert.AreEqual(avrgage, LinqExtensionMethods.PetConcaterClass.AverageAge(cats));
        }
        [TestMethod()]
        public void CountPets()
        {
            Pet[] cats = Pet.GetCats();
            Assert.AreEqual(LinqExtensionMethods.PetConcaterClass.CountPets(cats), cats.Length);
        }

        [TestMethod()]
        public void ExceptPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            List<Pet> petRef = new List<Pet>(); 
            Pet[] pets = LinqExtensionMethods.PetConcaterClass.Exceptpets(cats, dogs);
            foreach (Pet item in cats)
            {
                if (!dogs.Contains(item))
                {
                    petRef.Add(item);
                }
            }
            Assert.AreEqual(pets.Length, petRef.Count);
        }

        [TestMethod()]
        public void IntersectPets()
        {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            List<Pet> petRef = new List<Pet>();
            Pet[] pets = LinqExtensionMethods.PetConcaterClass.Intersectpets(cats, dogs);
            foreach (Pet item in cats)
            {
                if (dogs.Contains(item))
                {
                    petRef.Add(item);
                }
            }
            Assert.AreEqual(pets.Length, petRef.Count);
        }
        [TestMethod()]
        public void MaxPets()
        {
            Pet[] cats = Pet.GetCats();
            int maxAge = 0;
            foreach (Pet item in cats)
            {
                if (item.Age > maxAge)
                {
                    maxAge = item.Age;
                }
            }
            Assert.AreEqual(maxAge, LinqExtensionMethods.PetConcaterClass.MaxPets(cats));
        }

        [TestMethod()]
        public void MinPets()
        {
            Pet[] cats = Pet.GetCats();
            int minAge = cats[0].Age;
            foreach (Pet item in cats)
            {
                if (item.Age < minAge)
                {
                    minAge = item.Age;
                }
            }
            Assert.AreEqual(minAge, LinqExtensionMethods.PetConcaterClass.MinPets(cats));
        }
        [TestMethod()]
        public void SumPets()
        {
            Pet[] cats = Pet.GetCats();
            int sumAge = 0;
            foreach (Pet item in cats)
            {
                sumAge += item.Age;
            }
            Assert.AreEqual(sumAge, LinqExtensionMethods.PetConcaterClass.SumPets(cats));
        }
        [TestMethod()]
        public void UnionPets()
        {
            
            List<Pet> cats = Pet.GetCats().ToList();
            List<Pet> dogs = Pet.GetDogs().ToList();
            List<Pet> UnionList = new List<Pet>(dogs);
            IEnumerable<Pet> union = LinqExtensionMethods.PetConcaterClass.UnionPets(cats.ToArray(), dogs.ToArray());
            
            for (int i = 0; i < cats.Count; i++)
            {
                if (!UnionList.Contains(cats[i]))
                {
                    UnionList.Add(cats[i]);
                }
            }

            Assert.AreEqual(union.Count(), UnionList.Count);
            
        }


    }
 }