﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples
{
    public class NameGenerator
    {
        static string[] names = { "Benedikt", "Raphael",
            "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
        static string[] pupils = { "Nora", "Simon", "Markus", "Christoph",
            "Josef", "Benjamin", "Daniel", "Benedikt", "Peter", "Gregor",
            "Dominik", "Simon", "Peter", "Alexander", "Daniel", "Christina", "Raphael"};
        public static string[] GetNames()
        {
            return names;
        }
        public static string[] GetPupils()
        {
            return pupils;
        }
    }
    public class Pet
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public static Pet[] GetCats()
        {
            Pet[] cats = { new Pet { Name="Barley", Age=8 },
                       new Pet { Name="Boots", Age=4 },
                       new Pet { Name="Whiskers", Age=1 } };
            return cats;
        }
        public static Pet[] GetDogs()
        {
            Pet[] dogs = { new Pet { Name="Bounder", Age=3 },
                        new Pet { Name="Snoopy", Age=14 },
                        new Pet { Name="Fido", Age=9 } };
            return dogs;
        }
    }
    public class LinqExtensionMethods
    {
        #region Easy Extension Methods
        public static string[] SortNames(string[] names)
        {
            IEnumerable<string> result= names.OrderBy(x => x); //order by
            return result.ToArray();
        }

        public static string[] ConcatNames(string[] names, string[] pupils)
        {
            return names.Concat(pupils).ToArray(); //concat
        }

        public static string[] RemoveRedundant(string[] names)
        {
            return names.Distinct().ToArray(); //Distinct
        }

        public static string[] UnionNames(string[] names, string[] pupils)
        {
            return names.Union(pupils).ToArray(); //Union
        }

        public static string[] ReverseNames(string[] names)
        {
            return names.Reverse().ToArray(); //Reverse
        }
        #endregion

        #region Concat_PetArrays
       
        public class PetConcaterClass
        {
            public static IEnumerable<string> ConcatArrays(Pet[] cats, Pet[] dogs)
            {
                IEnumerable<string> query= cats.Select(c=> c.Name).Concat(dogs.Select(d=>d.Name)); //Select cats&dogs then Concat
                return query.ToArray();
            }
            public static double AverageAge(Pet[] p)
            {
                return p.Select(x => x.Age).Average();
            }
            public static int CountPets(Pet[] p)
            {
                return p.Count();
            }
            public static Pet[] Exceptpets(Pet[] p, Pet []q)
            {
                return p.Except(q).ToArray();

            }
            public static Pet[] Intersectpets(Pet[] p, Pet[] q)
            {
                return p.Intersect(q).ToArray();
            }
            public static int MaxPets(Pet []p)
            {
                return p.Select(x => x.Age).Max();
            }
            public static int MinPets(Pet[] p)
            {
                return p.Select(x => x.Age).Min();
            }
            public static int SumPets(Pet[] p)
            {
                return p.Select(x => x.Age).Sum();
            }
            public static Pet[] UnionPets(Pet[] p, Pet[] q)
            {
                return p.Union(q).ToArray();
            }

        }
        
        #endregion

    }
}
