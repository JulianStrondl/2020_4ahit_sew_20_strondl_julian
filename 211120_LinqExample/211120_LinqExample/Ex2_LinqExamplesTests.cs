﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class LinqExamplesTests
    {
        [TestMethod()]
        public void SelectScoresGreaterTest()
        {
            int[] scores = { 90, 71, 82, 93, 75, 82, 100, 39, 99 };
            int greaterThan = 80;

            //Test Score Funktions
            int[] resultF = Ex2_LinqQueryExamples.FSelectScoresGreater(scores, greaterThan);
            int[] resultM= Ex2_LinqQueryExamples.MSelectScoresGreater(scores, greaterThan); 
            int[] expectedResult = new int[] { 100, 99, 93, 90, 82, 82 };
            for (int i = 0; i < resultM.Length; i++)
            {
                Assert.AreEqual(expectedResult[i], resultM[i]);
                Assert.AreEqual(expectedResult[i], resultF[i]);
            }
        }

        [TestMethod()]
        public void SortWordsTest()
        {
            string[] words = { "Affenbrotbaum", "Chiasamen",
                       "Acai", "Matcha", "Flohsamenschalen" };
            Console.WriteLine("\nSort Words Alphabetically");
            string[] names = Ex2_LinqQueryExamples.MSortWords(words);

            //Test your SortMethods
            string[] resultFLengthOrder = Ex2_LinqQueryExamples.FSortByWordLengthDesc(words); 
            string[] resultFOrder = Ex2_LinqQueryExamples.FSortWords(words);  
            string[] resultMLengthOrder = Ex2_LinqQueryExamples.MSortByWordLengthDesc(words);
            string[] resultMOrder = Ex2_LinqQueryExamples.MSortWords(words);
            string[] expectedLenghtOrder = { "Flohsamenschalen",
                "Affenbrotbaum", "Chiasamen", "Matcha", "Acai" };
            string[] expectedWordOrder = { "Acai", "Affenbrotbaum",
                "Chiasamen", "Flohsamenschalen", "Matcha" };

            for (int i = 0; i < expectedLenghtOrder.Length; i++)
            {
                Assert.AreEqual(expectedLenghtOrder[i], resultFLengthOrder[i]);
                Assert.AreEqual(expectedWordOrder[i], resultFOrder[i]);
                Assert.AreEqual(expectedLenghtOrder[i], resultMLengthOrder[i]);
                Assert.AreEqual(expectedWordOrder[i], resultMOrder[i]);
            }

        }
   
        [TestMethod()]
        public void OrderByLenghtThenByAlphabetTest()
        {
            string[] words = { "Affenbrotbaum", "Chiasamen",
                       "Acai", "Matcha", "Flohsamenschalen" };

            string[] expectedWordOrder = {  "Acai","Matcha", "Chiasamen","Affenbrotbaum"
                      ,  "Flohsamenschalen" };
            //write test case
            string[] w = Ex2_LinqQueryExamples.FOrderByLenghtThenByAlphabet(words);
            string[] w2 = Ex2_LinqQueryExamples.MOrderByLenghtThenByAlphabet(words);

            for (int i = 0; i < expectedWordOrder.Length; i++)
            {
                Assert.AreEqual(expectedWordOrder[i], w[i]);
                
                Assert.AreEqual(expectedWordOrder[i], w2[i]);
            }
        }
    }
}
