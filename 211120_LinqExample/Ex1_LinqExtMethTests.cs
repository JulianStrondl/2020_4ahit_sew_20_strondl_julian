﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples.Tests
{
    [TestClass()]
    public class LinqExtensionMethodsTests
    {
        [TestMethod()]
        public void SortNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            //Test SortNames Method
            string[] sorted = new string[0];
            for (int i = 0; i < sorted.Length - 1; i++)
            {
                //Use the String.Compare() to test your SortNames-Method
            }
            Assert.Fail("Unit Test is missing");
        }

        [TestMethod()]
        public void ConcatNamesTest()
        {
            string[] names = NameGenerator.GetNames();
            string[] pupils = NameGenerator.GetPupils();

            //Test ConcatNames Method
            string[] result;

            //Use the Length of the collections: names+pupils == result 
            Assert.Fail("Unit Test is missing");
        }

        [TestMethod()]
        public void RemoveRedundantTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());
            string[] concat ;
            
            //Test remove redundant
            List<string> result = new List<string>(); 

            //if you take out element by element - every one should be unique
            //so there should not be a second one with the same value in the list
            while (result.Count > 1)
            {
            }
            Assert.Fail("Redundat values are existing");
        }

        [TestMethod()]
        public void UnionNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            string[] pupils = LinqExtensionMethods.SortNames(NameGenerator.GetPupils());

            //Use Concat & Remove 
            string[] concat ;
            string[] result ;

            //Test UnionNames 
            string[] union;

            //Both collections should have the same amount of elements (result == union)
            Assert.Fail("Unit Test is missing");
        }

        [TestMethod()]
        public void ReverseNamesTest()
        {
            string[] names = LinqExtensionMethods.SortNames(NameGenerator.GetNames());
            //Test ReverseName Method
            string[] result ;

            //First element should now be the last element
            Assert.Fail("Unit Test is missing");
        }
    }

    [TestClass()]
    public class LinqPetMethodsTests { 
        //TODO: Test your own PetMethods
        [TestMethod]
        public void ConcatPets() {
            Pet[] cats = Pet.GetCats();
            Pet[] dogs = Pet.GetDogs();
            IEnumerable<string> query = LinqExtensionMethods.PetConcaterClass.ConcatArrays(cats, dogs);

            //Überprüfung hinzufügen
            Assert.Fail();

            Console.WriteLine("Concat Pets into a collection of Names:");
            foreach (var e in query)
                Console.Write("{0}, ", e);
            Console.WriteLine("\n");
        }
    }
 }