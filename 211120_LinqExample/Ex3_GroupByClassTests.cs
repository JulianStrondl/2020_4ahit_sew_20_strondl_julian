﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinqAdancedExamples.Ex3_LinqGroupingExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples.Ex3_LinqGroupingExample.Tests
{
    [TestClass()]
    public class GroupByClassTests
    {
      
        [TestMethod()]
        public void GroupingByFirstLetterDescendingTest()
        {
            Assert.Fail();
            //Change the TestMethod to a Unittest
            {
                /*
                       static string[] foodList = { "carrots", "corn", "curcumber", "cabbage", "broccoli", "beans",
                        "barley", "garlic","ginger", "onions", "olives", "orache", "orka", "radicchio",
                        "parsnip", "pinto beans", "pumpkin" };

                        IDictionary<char, int> result =
                            GroupingByFirstLetterDescending(foodList);

                        foreach (var item in result)
                        {
                            Console.WriteLine("Buchstabe: {0}, Anzahl: {1}", item.Key, item.Value);
                        }


                    string[] names = { "Benedikt", "Raphael", "Sonja", "Simon", "Alexa", "Alex", "Benjamin" };
                        Program.PrintList(names, "Grouping Names by First Letter");
                        IDictionary<char, int> groupedFirstLetter =
                            GroupingByFirstLetterDescending(names);

                        foreach (var letter in groupedFirstLetter)
                        {
                            Console.WriteLine("{0}:{1}", letter.Key.ToString(), letter.Value);
                        }
                */
            }
        }
    }
}