﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqAdancedExamples
{
    #region GenerateObjects for Linq Examples
    public class Car
    {
        public string Name { get; set; }
        public int HorsePower { get; set; }

        public static Car[] GenerateCars()
        {
            return new Car[] {
                        new Car { Name = "Super Car", HorsePower = 215 },
                        new Car { Name = "Economy Car", HorsePower = 75 },
                        new Car { Name = "Family Car", HorsePower = 145 },
                        };
        }
    }

    #endregion
    
    public class Ex2_LinqQueryExamples
    {
        #region Projection - Select & SelectMany
        public static void SelectRoundedValues()
        {
            decimal[] numbers = { 3.4M, 8.33M, 5.225M };

            var result = numbers; //write your query

            Console.WriteLine("Numbers rounded down:");
            foreach (int number in result)
                Console.WriteLine(number);
        }
        public static void SelectAnonymousType()
        {
            double[] angles = { 30D, 60D, 90D }; // Angles in radians
           /*
            var result ; //write your query 

            Console.WriteLine("\nCalculated values:");
            foreach (var res in result)
                Console.WriteLine(String.Format("Angle {0}: Cos = {1}, Sin = {2}", 
                    res.Angle, res.Cos, res.Sin));
            Console.WriteLine();
            */
        }

        public static void SelectWithIndex()
        {
            string[] words = { "one", "two", "three" };
            /*
            var result ; //write your query 

            Console.WriteLine("Words with index and value:");
            foreach (var word in result)
                Console.WriteLine(String.Format("Index {0} is {1}",
                    word.Index, word.Value));
            Console.WriteLine();
            */
        }
        public static void SelectManyForCrossJoin()
        {
            string[] fruits = { "Grape", "Orange", "Apple" };
            int[] amounts = { 1, 2, 3 };
            /*
            var result; 

            Console.WriteLine("Selecting all values from each array, and mixing them:");
            foreach (var o in result)
                Console.WriteLine(o.Fruit + ", " + o.Amount);
            Console.WriteLine();
            */
        }
        #endregion

        #region Orderby & Where
        //Select all numbers greater than "value" in scores
        //return the result in decending order 
        public static int[] FSelectScoresGreater(int[] scores, int value)
        {
            /*
            // Use Query Syntax
            var scoreQuery = null;//query variable

            return scoreQuery.ToArray();
            */
            return null;
        }
        public static int[] MSelectScoresGreater(int[] scores, int value)
        {
            /*
            // Use Method Syntax 
            var scoreQuery = null;
            return scoreQuery.ToArray();
            */
            return null;
        }
        #endregion

        #region OrderBy Ascending Or Descending
        public static string[] FSortWords(string[] words)
        {
            // Use Query Syntax
            IEnumerable<string> query = from word in words
                                        orderby word ascending
                                        select word;
            return query.ToArray();
        }
        public static string[] MSortWords(string[] words)
        {
            // Use Method Syntax
            IEnumerable<string> query = words.OrderBy(s=>s);
            return query.ToArray();
        }

        public static string[] FSortByWordLengthDesc(string[] words)
        {
            //Sort Words by wordlength descending
            //Use Query Syntax
            IEnumerable<string> query = null; 
            return query.ToArray();
        }
        public static string[] MSortByWordLengthDesc(string[] words)
        {
            //Sort Words by wordlength descending
            //Use Method Syntax
            IEnumerable<string> query = null;
            return query.ToArray();
        }
        #endregion
        
        #region OrderBy_Thenby
        public static string[] FOrderByLenghtThenByAlphabet(string[] words)
        {
            // Sort the strings first by their length and then alphabetically 
            // Use query Syntax
            IEnumerable<string> query = null;

            return query.ToArray();
        }
        public static string[] MOrderByLenghtThenByAlphabet(string[] words)
        {
            // Sort the strings first by their length and then 
            // alphabetically by passing the identity selector function.
            // Use Method Syntax
            IEnumerable<string> query = null;
                
            return query.ToArray();
        }
        #endregion
    
        #region OrderBy With Objects (Cars)
        public class CarOrderByExample
        {
            public static Car[] FOrderCarByHorsepower(Car[] cars)
            {
                /*
                // Order by Horsepower 
                // Use Query Syntax
                var result = null; 
                return result.ToArray();
                */
                return null;
            }

            public static Car[] MOrderCarByHorsepower(Car[] cars)
            {
                /*
                // Order by Horsepower 
                // Use Method Syntax
                var result; 
                return result.ToArray();
                */
                return null;
            }
        }
        #endregion

    }

}