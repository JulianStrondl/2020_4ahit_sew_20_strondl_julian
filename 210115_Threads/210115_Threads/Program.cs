﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210115_Threads
{
    class Program
    {
        static bool abschr = true;
        static void Main(string[] args)
        {
            //SimpleThreadCreation();
            //ThreadsParams();
            //ThreadInformation();
            // RaceCondition();
            //SyncJoin();
            //SyncLock();
            //SyncMonitor();
            // Mutex1();
            Mutex2();
        }
        #region SimpleThreadZeigs
        static void SimpleThreadCreation()
        {
            Thread t = new Thread(myFun);
            t.Start();
            Console.WriteLine("Main Thread");
        }
        static void myFun()
        {
            Console.WriteLine("Running on another thread");
        }
        #endregion
        #region ThreadsMitparameter
        static void ThreadsParams()
        {
            ParameterizedThreadStart pts = new ParameterizedThreadStart(method);
            Thread t = new Thread(pts);
            t.Start(69);

            Thread t1 = new Thread(delegate () { Console.WriteLine("Frech"); });
            t1.Start();
        }
        static void method(Object param)
        {
            Console.WriteLine(param);
        }
        #endregion
        #region threadInfo
        static void ThreadInformation()
        {
            Console.WriteLine("Thread Infos");
            Thread t = Thread.CurrentThread;
            t.Name = "prim_Thread";
            Console.WriteLine("Thread Name "+ t.Name);
            Console.WriteLine("Thread Status "+ t.IsAlive);
            Console.WriteLine("Priority "+Thread.CurrentContext.ContextID);
            Console.WriteLine("Current application domain "+ Thread.GetDomain().FriendlyName);
            Console.WriteLine("Abschreibübung "+abschr);
        }
        #endregion
        #region RaceCondition
        private static int counter;
        static void RaceCondition()
        {
            Thread t1 = new Thread(PrintPlus);
            t1.Start();
            Thread t2 = new Thread(PrintStar);
           
            t2.Start();
        }
        static void PrintStar()
        {
            for (counter = 0; counter < 5; counter++)
            {
                Console.WriteLine("*");
            }
        }
        static void PrintPlus()
        {
            for (counter = 0; counter < 5; counter++)
            {
                Console.WriteLine("+");
            }
        }
        #endregion
        #region SyncJoin
        static void SyncJoin()
        {
            //join lässt einen Thread auf die untergeordneten Warten
            Thread t1 = new Thread(PrintStar);
            t1.Start();
            t1.Join();
            Console.WriteLine();
            Thread t2 = new Thread(PrintPlus);
            t2.Start();
            t2.Join();

            Console.WriteLine("Main Thread");
        }
        #endregion
        #region SyncLock
        static object locker = new object();
        static void SyncLock()
        {
            //Lock versichert, dass nur ein Thread ausgeführt wird
            new Thread(PrintStarLock).Start();
            new Thread(PrintPlusLock).Start();
        }
        static void PrintStarLock()
        {
            lock (locker)
            {
                 for (counter = 0; counter < 5; counter++)
                  {
                    Console.WriteLine("*");
                 }
            }
            
        }
        static void PrintPlusLock()
        {
            lock (locker)
            {
                for (counter = 0; counter < 5; counter++)
                {
                    Console.WriteLine("+");
                }

            }
        }
        #endregion
        #region SyncMonitor
        static void SyncMonitor()
        {
            new Thread(PrintStarMonit).Start();
            new Thread(PrintPlusmonit).Start();

        }
        static void PrintStarMonit()
        {
            Monitor.Enter(locker);
            try
            {
                for (counter = 0; counter < 5; counter++)
                {
                    Console.WriteLine("*");
                }
            }
            finally
            {
                Monitor.Exit(locker);
            }

        }
        static void PrintPlusmonit()
        {
            Monitor.Enter(locker);
            try
            {
                for (counter = 0; counter < 5; counter++)
                {
                    Console.WriteLine("+");
                }
            }
            finally
            {
                Monitor.Exit(locker);
            }
        }
        #endregion

        #region SyncMutex
        private static Mutex mut = new Mutex();
        private const int numIterations = 1;
        private const int numThreads = 3;

        static void Mutex1()
        {
            for (int i = 0; i < numThreads; i++)
            {
                Thread newThread = new Thread(new ThreadStart(ThreadProc));
                newThread.Name = String.Format("Thread " + (i + 1));
                newThread.Start();
            }
        }
        static void ThreadProc()
        {
            
        }
        
        #endregion
        #region SyncMutex2
        

        static void Mutex2()
        {
            StartThreads();
        }
       private static void StartThreads()
        {
            for (int i = 0; i < numThreads; i++)
            {
                Thread newThread = new Thread(new ThreadStart(Threadproc));
                newThread.Name = String.Format("Thread {0}", i + 1);
                newThread.Start();
            }
        }
        static void Threadproc()
        {
            for (int i = 0; i < numIterations; i++)
            {
                UseResource1();
            }
        }
        private static void UseResource1()
        {
            Console.WriteLine("{0} is requesting the mutex", Thread.CurrentThread.Name);
            if (mut.WaitOne(1000))
            {
                Console.WriteLine("{0} has entered the protected area", Thread.CurrentThread.Name);

                Thread.Sleep(5000);

                Console.WriteLine("{0} is leaving the protected area", Thread.CurrentThread.Name);

                mut.ReleaseMutex();
                Console.WriteLine("{0} has released the mutex", Thread.CurrentThread.Name);
            }
            else
            {
                Console.WriteLine("{0} will not acquire the mutex", Thread.CurrentThread.Name);
            }
        }
        ~Program()
        {
            mut.Dispose();
        }
        #endregion
    }
}
