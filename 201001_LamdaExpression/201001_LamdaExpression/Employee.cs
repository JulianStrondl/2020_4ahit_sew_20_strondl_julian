﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201001_LamdaExpression
{
    class Employee
    {
        public string Name { get; private set; }
        public int Age { get; }
        public Employee(string name, int age)
        {

            Name = name;

            Age = age;

        }
    }
}
