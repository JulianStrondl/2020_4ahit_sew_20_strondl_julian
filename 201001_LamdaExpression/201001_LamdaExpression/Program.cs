﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201001_LamdaExpression
{
    class Program
    {
         static void Main(string[] args)
        {
            Func<int, int, int> check = (a, b) => a > b ? b : a;

            
            //MyClass.Start();
            //List<Employee> employees = new List<Employee>()
            //{
            //    new Employee("Fritz", 45),
            //    new Employee("Brigitte", 36),
            //    new Employee("Hans", 41),
            //    new Employee("Jürgen", 27)
            //};
            //List<Employee> employeesOver35 = employees.FindAll((e) => e.Age > 35 );
            //PrintList(employeesOver35);
            //Console.WriteLine();
            //employeesOver35 = FindAll<Employee>(employees, (e) => e.Age > 35 );
            //PrintList(employeesOver35);
            //Console.WriteLine();
            //employeesOver35 = employees.FindAll(delegate (Employee e) { return e.Age > 35; });
            //PrintList(employeesOver35);
            //Console.WriteLine();
            //employeesOver35 = FindAll<Employee>(employees, delegate (Employee e) { return e.Age > 35; });
            //PrintList(employeesOver35);

        }

        static List<T> FindAll<T>(List<T> ls, Func<T,bool> pr)
        {
            return ls.Where(pr).ToList();
        }
        static void PrintList(List<Employee> ls)
        {
            foreach (Employee item in ls)
            {
                Console.WriteLine($"{item.Name} Age:{item.Age}");
            }
        }
        



    }
    class MyClass
    {
        delegate void MyDelegate(String s);
        public static void Start()
        {
            MyDelegate a = new MyDelegate(Hello);

            MyDelegate b = new MyDelegate(Goodbye);

            MyDelegate c = a + b - a - b + b + a;

            MyDelegate d = c - a;
            c += d;
            d += (f) => { Console.WriteLine($"Tschüss {f.Substring(f.Length / 2)}"); };


            Console.WriteLine("--- a ---");

            a("A");

            Console.WriteLine("--- b ---");

            b("B");

            Console.WriteLine("--- c ---");

            c("C");

            Console.WriteLine("--- d ---");

            d("DA");
        }
        static void Hello(String s)
        {

            Console.WriteLine("Hello, {0}!", s);

        }

        static void Goodbye(String s)
        {

            Console.WriteLine("   Goodbye, {0}!", s);

        }
    }
}
