﻿using System;
using System.Linq;

namespace _210215_EFC_Foobar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entity Framework Example");
            using (var context= new FooBarContext())
            {
                var f01 = new Foo() { Name = "Karkus" };
                var f02 = new Foo() { Name = "Hauns" };
                context.Foos.Add(f01);
                context.Foos.Add(f02);
                var bo = new Bar()
                {
                    BarId = 0,
                    Name = "Schorsch"
                };
                context.Bars.Add(bo);
                context.SaveChanges();
                Console.WriteLine(context.Foos.First());
                Console.WriteLine(context.Bars.First());

            }
        }
    }
}
