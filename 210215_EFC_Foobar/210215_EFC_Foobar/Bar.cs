﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _210215_EFC_Foobar
{
    class Bar
    {
       public int BarId { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
