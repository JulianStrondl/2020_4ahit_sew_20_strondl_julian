﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace _210215_EFC_Foobar
{
    class FooBarContext:DbContext
    {
        public FooBarContext()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;
                                            Database=Foobar;Trusted_Connection=True;");
        }
        public DbSet<Foo> Foos { get; set; }
        public DbSet<Bar> Bars { get; set; }

    }
}
