﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _210118_TaskParallel
{
    class Program
    {
        static void Main(string[] args)
        {
            //For vs Parallel
            //Jede Schleifeniteration wird auf dem gleichen Thread ausgeführt und muss daher immer auf die vorherige warten
            Console.WriteLine("For");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Sequential iteration on index {0} running on thread {1}", i,Thread.CurrentThread.ManagedThreadId );
                Thread.Sleep(400);
            }
            //Hierbei wird jede Iteration dieser Schleife auf einen neuen Thread ausgelagert und ist somit sehr viel schneller
            Console.WriteLine("ParallelFor");
            Parallel.For(0, 10, i =>
             {
                 Console.WriteLine("Sequential iteration on index {0} running on thread {1}", i, Thread.CurrentThread.ManagedThreadId);
                 Thread.Sleep(400);
             });
        }
    }
}
