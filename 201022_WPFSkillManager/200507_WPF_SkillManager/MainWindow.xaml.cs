﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _200507_WPF_SkillManager
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Skill> skills = new List<Skill>()
            {
                new Skill(){ Name="C#", Percentage=80},
                new Skill(){ Name="Java", Percentage=50},
                new Skill(){ Name="Python", Percentage=10},
                new Skill(){ Name="Entity Framework", Percentage=80},
                new Skill(){ Name="Angular", Percentage=40},
                new Skill(){ Name="C#", Percentage=80},

            };
            var skillVM = DataContext as SkillsVM;
            skillVM.AddSkills(skills);
        }
    }
}
