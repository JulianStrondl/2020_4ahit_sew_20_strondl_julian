﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200507_WPF_SkillManager
{
    class AddSkillCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        SkillsVM parent;
        public AddSkillCommand(SkillsVM vm)
        {
            parent = vm;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.AddSkill();
        }
    }
}
