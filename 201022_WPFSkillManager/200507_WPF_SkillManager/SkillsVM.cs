﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;

namespace _200507_WPF_SkillManager
{
    class SkillsVM : INotifyPropertyChanged
    {
        Skill helpskill;
        public SkillsVM()
        {
            _skills = new ObservableCollection<Skill>();
            AddSkillCommand = new AddSkillCommand(this);
            AddPercentageCommand = new AddPercentageCommand(this);
            RemovePercentageCommand = new RemovePercentageCommand(this);
            skill = new Skill("",0);
            
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private Skill skill;
        public Skill Skill
        {
            get
            {
                return skill;
            }
            set
            {
                skill = value;
                OnPropertyChange("Skill");
            }
        }
        private Skill selectedSkill;
        public Skill SelectedSkill
        {
            get
            {
                return selectedSkill;
            }
            set
            {
                selectedSkill = value;
                OnPropertyChange();
            }
        }
        ObservableCollection<Skill> _skills;
        public ObservableCollection<Skill> Skills
        {
            get
            {
                return _skills;
            }
            set
            {
                _skills = value;
                OnPropertyChange();
            }
        }
        
        public void AddSkills(List<Skill> skills)
        {
            foreach (Skill item in skills)
            {
                Skills.Add(item);
            }
        }
        public ICommand AddSkillCommand { get; set; }
        public void AddSkill()
         {
            helpskill = new Skill(Skill.Name, Skill.Percentage);
            Skill.Name = null;
            Skills.Add(helpskill);
         }
        public ICommand AddPercentageCommand { get; set; }
        public void AddPercentage()
        {
            //MessageBox.Show(SelectedSkill.Percentage.ToString());

             int i = Skills.IndexOf(SelectedSkill);
             Skills[i].Percentage++;
             RefreshList();
            SelectedSkill = Skills[i];
            //SelectedSkill.Percentage++;



        }
        public ICommand RemovePercentageCommand { get; set; }
        public void RemovePercentage()
        {
            //MessageBox.Show(SelectedSkill.Name);
            int i = Skills.IndexOf(SelectedSkill);
            Skills[i].Percentage--;
            RefreshList();
            SelectedSkill = Skills[i];

            
            
        }
         void OnPropertyChange([CallerMemberName]string property =null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        void RefreshList()
        {
            List<Skill> helpList = Skills.ToList<Skill>();
            Skills = new ObservableCollection<Skill>();
            AddSkills(helpList);
        }
        
    }
}
