﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200507_WPF_SkillManager
{
    class Skill
    {
        
        public string Name { get; set; }
        int percentage;
        public Skill() { }

        public Skill(string name, int percentage)
        {
            Name = name;
            Percentage = percentage;
        }

        public int Percentage
        {
            get
            {
                return percentage;
            }
            set
            {
                
                    percentage = value;
                

            }
        }
        


    }
}
