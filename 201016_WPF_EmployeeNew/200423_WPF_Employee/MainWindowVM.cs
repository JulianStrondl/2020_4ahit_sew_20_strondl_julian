﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace _200423_WPF_Employee
{
    class MainWindowVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        ObservableCollection<Employee> _employees;
        public ObservableCollection<Employee> Employees
        {
            get
            {
                return _employees;
            }
            set
            {
               _employees = value;
                OnPropertyChanged("Employees");
            }
        }
        Employee selItem;
        public Employee SelItem
        {
            get
            {
                return selItem;
            }
            set
            {
                selItem = value;
                OnPropertyChanged("SelItem");
            }
        }
        
       
        public virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged !=null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
            
        }
        public MainWindowVM()
        {
            ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
            employees.Add(new Employee
            {
                MemberID = 1,
                Name = "John Hancock",
                Department = "IT",
                Phone = "31234743",
                Email = @"John.Hancock@Company.com",
                Salary
            = "3450.44"
            });
            employees.Add(new Employee
            {
                MemberID = 2,
                Name = "Jane Hayes",
                Department = "Sales",
                Phone = "31234744",
                Email = @"Jane.Hayes@Company.com",
                Salary
            = "3700"
            });
            employees.Add(new Employee
            {
                MemberID = 3,
                Name = "Larry Jones",
                Department = "Marketing",
                Phone = "31234745",
                Email = @"Larry.Jones@Company.com",
                Salary = "3000"
            });
            employees.Add(new Employee
            {
                MemberID = 4,
                Name = "Patricia Palce",
                Department = "Secretary",
                Phone = "31234746",
                Email =
            @"Patricia.Palce@Company.com",
                Salary = "2900"
            });
            employees.Add(new Employee
            {
                MemberID = 5,
                Name = "Jean L. Trickard",
                Department = "Director",
                Phone = "31234747",
                Email = @"Jean.L.Tricard@Company.com",
                Salary = "5400"
            });
            Employees = employees;
        }

    }
    public class Employee
    {
        public int MemberID { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Salary { get; set; }
        public override string ToString()
        {
            return $"MemberID: {MemberID}   Name: {Name}  Department: {Department}   Phone:{Phone} Email: {Email}   Salary:{Salary}";
        }
    }
}
