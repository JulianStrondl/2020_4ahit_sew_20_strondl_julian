﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200924_LambdaExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            //Schreibe eine Func mithilfe von Lambda Expressions, die 3 strings aneinander anfügt
            Func<string, string, string, string> concat = (a, b, c) => a + b + c;
            Console.WriteLine(concat("A", "B", "C"));
            //Erstelle ein Func mithilfe von Lambda Expressions, dass prüft ob zwei Zahlen gleich sind
            Func<int, int, bool> eq = (a, b) => a == b;
            Console.WriteLine(eq(1, 6));
            //Prüfe mit einem Predicate ob die der Punkt der in der Klasse Point dargestellt im ersten Quadranten sind (x <0 und Y>0)
            Predicate<Point> CheckForfirstQ = (a) => a.X < 0 && a.Y > 0;
            Console.WriteLine(CheckForfirstQ(new Point(2, 6)));
            //Erstelle ein Func mit der alle items eines String Array zu einem großen String zusammengefügt werden
            Func<string[], string> concatArr = (a) =>{string x = "";for (int i = 0; i < a.Length; i++){x += a[i];}return x;};
            Console.WriteLine(concatArr(new string []{ "A", "B", "C" }));
        }

        class Point
        {
            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
            public int X { get; set; }
            public int Y { get; set; }

        }
    }
}
                     
