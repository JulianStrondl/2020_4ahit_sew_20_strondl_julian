﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using _201123_LinqStudentBook;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reflection;

namespace UnitTestLinqStudentBook
{
    [TestClass]
    public class BooktestClass
    {
        ILogger txtLogger = new TxtLogger("Booklog.txt");
        ILogger csvLogger = new CsvLogger("Booklog.csv");
        ILogger xmlLogger = new XMLLogger("Booklog.xml");
        ILogger MdfLogger = new MdfLogger();
        [TestMethod]
        public void AveragePrice()
        {
            List<Book> books = Book.GetBooks();
            double totalPrice = 0;
            foreach (var item in books)
            {
                totalPrice += item.Price;
            }
            double avgPrice = totalPrice / books.Count;
            try
            {
                Assert.AreEqual(avgPrice, BookRepository.AveragePrice(books));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString()) ;
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
            
            
        }
        [TestMethod]
        public void TotalPrice()
        {
            List<Book> books = Book.GetBooks();
            double totalPrice = 0;
            foreach (var item in books)
            {
                totalPrice += item.Price;
            }
            
            try
            {
                Assert.AreEqual(totalPrice, BookRepository.TotalPrice(books));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());

            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

                Assert.Fail();

            }


        }
        [TestMethod()]
        public void MaxPrice()
        {
            List<Book> books = Book.GetBooks();
            double maxPrice = books[0].Price;
            foreach (var item in books)
            {
                if (item.Price > maxPrice)
                {
                    maxPrice = item.Price;
                }
            }
            try
            {
                Assert.AreEqual(maxPrice, BookRepository.MaxPrice(books));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());

            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

                Assert.Fail();

            }
        }
        [TestMethod()]
        public void MinPrice()
        {
            List<Book> books = Book.GetBooks();
            double maxPrice = books[0].Price;
            foreach (var item in books)
            {
                if (item.Price < maxPrice)
                {
                    maxPrice = item.Price;
                }
            }
            try
            {
                Assert.AreEqual(maxPrice, BookRepository.MinPrice(books));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());

            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

                Assert.Fail();

            }
        }

        [TestMethod()]
        public void ToBookArray()
        {
            Book[] a = new Book[5];
            List<Book> books = Book.GetBooks();
            try
            {
                Assert.IsInstanceOfType(BookRepository.ToBookArray(books),a.GetType());
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), books.ToString());

            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                xmlLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                MdfLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

                Assert.Fail();

            }

        }
    }

    [TestClass]
    public class OrderRepository
    {
        ILogger txtLogger = new TxtLogger("OrderLog.txt");
        ILogger csvLogger = new CsvLogger("OrderLog.csv");
        [TestMethod()]
        public void TypeOfOrder()
        {
            List<Order> orders = Order.Getorders();
            List<Order> ordersref = Order.Getorders();
             try
            {
                CollectionAssert.AreEqual(orders, Orderrepository.TypeOfOrder(orders)); ;
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), orders.ToString());

            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();
            }
        }
    }

    [TestClass]
    public class SchoolTestClass
    {
        ILogger txtLogger = new TxtLogger("SchoolsLog.txt");
        ILogger csvLogger = new CsvLogger("SchoolsLog.csv");
        
        [TestMethod()]
        public void Takefirst()
        {
            int count = 3;
            List<School> schoolsref = new List<School>();
            List<School> schools = School.GetSchools();
            for (int i = 0; i < count; i++)
            {
                schoolsref.Add(schools[i]);
            }
            try
            {
                CollectionAssert.AreEqual(schoolsref, SchoolRepository.TakeFirst(schools,count));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsref.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");

                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }

        }

        [TestMethod()]
        public void SkipFirst()
        {
            int count = 2;
            List<School> schoolsref = new List<School>();
            List<School> schools = School.GetSchools();
            for (int i = 0; i < schools.Count; i++)
            {
                if (i >= count)
                {
                    schoolsref.Add(schools[i]);
                }
            }
            try
            {
                CollectionAssert.AreEqual(schoolsref, SchoolRepository.SkipFirst(schools, count));
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsref.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void ConcatSchools()
        {
            List<School> schools = School.GetSchools();
            List<School> schools2 = School.GetSchools2();
            List<School> schoolsconc = SchoolRepository.ConcatSchools(schools,schools2);

            for (int i = 0; i < schools2.Count; i++)
            {
                schools.Add(schools2[i]);
            }
            try
            {
                CollectionAssert.AreEqual(schoolsconc,schools );
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsconc.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void DisticntSchools()
        {
            List<School> schools = School.GetSchools();
            List<School> schools2 = School.GetSchools2();
            List<School> schoolsconc = SchoolRepository.DisctinctSchools(schools, schools2);

            for (int i = 0; i < schools2.Count; i++)
            {
                for (int j = 0; j < schools.Count; j++)
                {
                    if (schools[j]== schools2[i])
                    {
                        break;
                    }
                    
                }
                schools.Add(schools2[i]);
            }
            try
            {
                CollectionAssert.AreEqual(schoolsconc, schools);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsconc.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void ExceptSchools()
        {
            List<School> schools = School.GetSchools();
            List<School> schools2 = School.GetSchools2();
            List<School> schoolsconc = SchoolRepository.GetExceptSchools(schools, schools2);

            for (int i = 0; i < schools2.Count; i++)
            {
                for (int j = 0; j < schools.Count; j++)
                {
                    if (schools[j] != schools2[i])
                    {
                        break;
                    }
                    schools.Add(schools2[i]);
                }
            }
            try
            {
                CollectionAssert.AreEqual(schoolsconc, schools);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsconc.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void IntersectSchools()
        {
            List<School> schools = School.GetSchools();
            List<School> schools2 = School.GetSchools2();
            List<School> schoolsconc = SchoolRepository.GetIntersectSchools(schools, schools2);
            List<School> schoolsintersect = new List<School>();
            for (int i = 0; i < schools2.Count; i++)
            {
                for (int j = 0; j < schools.Count; j++)
                {
                    if (schools[j] != schools2[i])
                    {
                        break;
                    }
                    schoolsintersect.Add(schools2[i]);
                }
            }
            try
            {
                CollectionAssert.AreEqual(schoolsconc, schoolsintersect);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsconc.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void UnionSchoolstest()
        {
            List<School> schools = School.GetSchools();
            List<School> schools2 = School.GetSchools2();
            List<School> schoolsconc = SchoolRepository.ConcatSchools(schools, schools2);

            for (int i = 0; i < schools2.Count; i++)
            {
                schools.Add(schools2[i]);
            }
            try
            {
                CollectionAssert.AreEqual(schoolsconc, schools);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), schoolsconc.ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }

        }

    }


    [TestClass()]
    public class StudenTestClass
    {
        ILogger txtLogger = new TxtLogger("SchoolsLog.txt");
        ILogger csvLogger = new CsvLogger("SchoolsLog.csv");

        [TestMethod()]
        public void Countstudents()
        {
            List<Student> students = Student.GetStudents();

            try
            {
                Assert.AreEqual(students.Count, StudentRepository.CountStudents(students));    
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), StudentRepository.CountStudents(students).ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
        [TestMethod()]
        public void OrderStudentsbyLastnameTest()
        {
            List<Student> students = StudentRepository.OrderStudentsbyLastname(Student.GetStudents());
            
            List<Student> orderedStudents = new List<Student>
            {
                
                new Student(6, "Hans", "Bauer", 14, 311418),
                new Student(8, "Agathe", "Bauer", 12, 301091),
                new Student(2, "Sascha", "Huber", 13, 301091),
                new Student(1, "Paul", "Nagl", 17, 301447),
                new Student(4, "Tonu", "Polster", 17, 301081),
                new Student(5, "Axel", "Schweiß", 13, 311014),
                new Student(3, "Sebastian", "Short", 15, 301052),
                new Student(9, "Susi", "Spreiz", 14, 311429),
                new Student(0, "Klausi", "Wartmann", 19, 301447),
                new Student(7, "Donna", "Wetter", 15, 311418),
                new Student(10, "Anna", "Will", 19, 311429)



            };
            
            foreach (var item in students)
            {
                Console.WriteLine($"{item.Id} {item.FName} {item.LName} {item.Age} {item.SchoolId}");
            }
            Console.WriteLine();
            foreach (var item in orderedStudents)
            {
                Console.WriteLine($"{item.Id} {item.FName} {item.LName} {item.Age} {item.SchoolId}");
            }
            try
            {
                Assert.AreEqual(orderedStudents.Count, students.Count);
                //Assert.AreEqual(1, 1);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), StudentRepository.CountStudents(students).ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }

        }
        
        [TestMethod()]
        public void OrderStudentsbyLastnameFirsNameTest()
        {
            List<Student> students = Student.GetStudents();
            List<Student> orderedStudents = new List<Student>
            {
                new Student(8, "Agathe", "Bauer", 12, 301091),
                new Student(6, "Hans", "Bauer", 14, 311418),
                new Student(2, "Sascha", "Huber", 13, 301091),
                new Student(1, "Paul", "Nagl", 17, 301447),
                new Student(4, "Tonu", "Polster", 17, 301081),
                new Student(5, "Axel", "Schweiß", 13, 311014),
                new Student(3, "Sebastian", "Short", 15, 301052),
                new Student(9, "Susi", "Spreiz", 14, 311429),
                new Student(0, "Klausi", "Wartmann", 19, 301447),
                new Student(7, "Donna", "Wetter", 15, 311418),
                new Student(10, "Anna", "Will", 19, 311429)
            };
            try
            {
                Assert.AreEqual(orderedStudents.Count, StudentRepository.OrderStudentsbyLastnameThenByFirstName(students).Count);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), StudentRepository.CountStudents(students).ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }

        }
        [TestMethod()]
        public void OrderStudentsbyLastnameFirsNameDescTest()
        {
            List<Student> students = Student.GetStudents();
            List<Student> orderedStudents = new List<Student>
            {
                new Student(10, "Anna", "Will", 19, 311429),
                new Student(7, "Donna", "Wetter", 15, 311418),
                new Student(0, "Klausi", "Wartmann", 19, 301447),
                new Student(9, "Susi", "Spreiz", 14, 311429),
                new Student(3, "Sebastian", "Short", 15, 301052),
                new Student(5, "Axel", "Schweiß", 13, 311014),
                new Student(4, "Tonu", "Polster", 17, 301081),
                new Student(1, "Paul", "Nagl", 17, 301447),
                new Student(2, "Sascha", "Huber", 13, 301091),
                new Student(6, "Hans", "Bauer", 14, 311418),
                new Student(8, "Agathe", "Bauer", 12, 301091),

            };


            try
            {
                Assert.AreEqual(orderedStudents.Count, StudentRepository.OrderStudentsbyLastnameThenByFirstNameDescending(students).Count);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), StudentRepository.CountStudents(students).ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }

        }
        [TestMethod()]
        public void ReverStudentstest()
        {
            List<Student> students = Student.GetStudents();
            List<Student> studentsref = new List<Student>();

            for (int i = students.Count-1; i >=0; i--)
            {
                studentsref.Add(students[i]);
            }

            try
            {
                CollectionAssert.AreEqual(StudentRepository.ReverseStudents(students),studentsref);
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "succesful");
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), StudentRepository.CountStudents(students).ToString());
            }
            catch (AssertFailedException)
            {
                txtLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                csvLogger.Log(MethodBase.GetCurrentMethod().ToString(), "failed");
                Assert.Fail();

            }
        }
    }
}
