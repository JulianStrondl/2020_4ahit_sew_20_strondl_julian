﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class BookRepository
    {
        
        
        public static double AveragePrice(List<Book> books)
        {
            return books.Select(s => s.Price).Average();
        }
        public static double TotalPrice(List<Book> books)
        {
            return books.Select(s => s.Price).Sum();
            
        }
        public static double MaxPrice(List<Book> books)
        {
            return books.Select(s => s.Price).Max();
        }
        public static double MinPrice(List<Book> books)
        {
            return books.Select(s => s.Price).Min();
        }
        public static Book[] ToBookArray(List<Book> books)
        {
            return books.ToArray();
        }


    }
}
