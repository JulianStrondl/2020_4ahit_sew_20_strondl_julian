﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class Person
    {
        public Person(string fName, string lName, int age)
        {
            FName = fName;
            LName = lName;
            Age = age;
        }

        public string FName { get; set; }
        public string LName { get; set; }
        public int Age { get; set; }
    }
}
