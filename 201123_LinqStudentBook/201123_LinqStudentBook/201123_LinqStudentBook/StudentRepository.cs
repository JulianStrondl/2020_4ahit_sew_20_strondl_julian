﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
   public class StudentRepository
    {
        public static int CountStudents(List<Student> students)
        {
            return students.Count();
        }
        public static List<Student> OrderStudentsbyLastname(List<Student> students)
        {
            return students.OrderBy(s=> s.LName).ToList();
        }
        public static object JoinStudentsonSchool(List<Student> students, List<School> schools)
        {
            var query = from student in students
                        join school in schools
                            on student.SchoolId equals school.Id
                        select new
                        {
                            studentID = student.Id,
                            StudentName = student.Name,
                            schooldID = school.Id,
                            SchoolName = school.Name

                        };
            return query.ToList();
        }
        public static List<Student> OrderStudentsbyLastnameThenByFirstName(List<Student> students)
        {
            return students.OrderBy(s => s.LName).ThenBy(s=> s.FName).ToList();
        }
        public static Dictionary<int,Student> ConvertToDictionary(List<Student> students)
        {
            return students.ToDictionary(s => s.Id);
        }


        public static List<Student> ReverseStudents(List<Student> students)
        {
            students.Reverse();
            return students;
        }
        public static List<Student> OrderStudentsbyLastnameThenByFirstNameDescending(List<Student> students)
        {
            return students.OrderByDescending(s => s.LName).ThenByDescending(s => s.FName).ToList();
        }
    }
}
