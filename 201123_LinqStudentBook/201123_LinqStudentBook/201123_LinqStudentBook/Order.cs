﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class Order
    {
        public Order(int orderId, int studentId, int bookId, DateTime date)
        {
            OrderId = orderId;
            StudentId = studentId;
            BookId = bookId;
            Date = date;
        }

        public int OrderId { get; set; }
        public  int StudentId { get; set; }
        public int BookId {get;set;}
        public  DateTime Date { get; set; }

        public static List<Order> Getorders()
        {
            return new List<Order>
            {
                new Order(1,1,1,DateTime.Now),
                new Order(2,2,2,DateTime.Now),
                new Order(3,2,3,DateTime.Now),
                new Order(4,3,4,DateTime.Now),
                new Order(5,3,5,DateTime.Now),

            };
        }

        
    }
}
