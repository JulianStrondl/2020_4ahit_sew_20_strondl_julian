﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public interface ILogger
    {
        string path { get; set; }
        void Log(string message, string result);

    }
}
