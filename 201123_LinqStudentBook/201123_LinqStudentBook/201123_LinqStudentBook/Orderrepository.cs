﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class Orderrepository
    {
        public static List<Order> TypeOfOrder(List<Order> orders)
        {
            return orders.OfType<Order>().ToList();
        }
    }
}
