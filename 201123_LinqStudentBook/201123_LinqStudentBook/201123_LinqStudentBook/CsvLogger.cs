﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _201123_LinqStudentBook
{
    public class CsvLogger : ILogger
    {
        static int id = 0;
        public string path { get ; set ; }
        public CsvLogger(string path)
        {
            this.path = path;
        }

        public void Log(string message, string result)
        {
            using (StreamWriter ws = new StreamWriter(path,true))
            {
                ws.WriteLine($"{id};{message};{result}");
                
            }
        }
    }
}
