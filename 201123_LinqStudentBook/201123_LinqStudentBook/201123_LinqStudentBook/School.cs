﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class School
    {
        public School(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public static List<School> GetSchools()
        {
            return new List<School>
            {
                new School(301447, "HTL Krems"),
                new School(301091, "VS Krems"),
                new School(301052, "MS Krems"),
                new School(301081, "Mary Ward Krems"),
                new School(311014, "Poly Horn"),
                new School(311418, "BHAK Horn"),
                new School(311429, "HLW Horn")
            };
        }
        public static List<School> GetSchools2()
        {
            return new List<School>
    {
        new School(310438, "HTL Hollabrunn"),
        new School(311429, "HLW Horn"),
        new School(310015, "LBS Hollabrunn"),
        new School(311418, "BHAK Horn"),
        new School(310016, "BRG Hollabrunn"),
        new School(310710, "LFS Hollabrunn"),
        new School(311014, "Poly Horn")
    };
        }



    }
}
