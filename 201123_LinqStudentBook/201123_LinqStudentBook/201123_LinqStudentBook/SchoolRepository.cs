﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class SchoolRepository
    {
        public static List<School> TakeFirst(List<School> schools, int count) {
            return schools.Take(count).ToList();
        }
        public static List<School> SkipFirst(List<School> schools, int count)
        {
            return schools.Skip(count).ToList();
        }

        public static List<School> ConcatSchools(List<School> schools, List<School> schools2)
        {
            return schools.Concat(schools2).ToList();
        }
        public static List<School> DisctinctSchools(List<School> schools, List<School> schools2)
        {
            return schools.Concat(schools2).Distinct().ToList();
        }
        public static List<School> GetExceptSchools(List<School> schools, List<School> schools2)
        {
            return schools.Except(schools2).ToList();
        }
        public static List<School> GetIntersectSchools(List<School> schools, List<School> schools2)
        {
            return schools.Intersect(schools2).ToList();
        }
        public static List<School> GetUnionSchools(List<School> schools, List<School> schools2)
        {
            return schools.Union(schools2).ToList();
        }
        
    }
}
