﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class Student:Person
    {
        public Student(int Id, string fName, string lName, int age, int SchoolId) : base(fName, lName, age)
        {
            this.Id = Id;
            this.SchoolId = SchoolId;
        }

        public int Id { get; set; }
        public string Name {
            get
            {
                return $"{base.FName} {LName}";
            }
        }
        public int SchoolId { get; set; }
        public static List<Student> GetStudents()
        {
            return new List<Student>
            {
                new Student(6, "Hans", "Bauer", 14, 311418),
                new Student(8, "Agathe", "Bauer", 12, 301091),
                new Student(2, "Sascha", "Huber", 13, 301091),
                new Student(1, "Paul", "Nagl", 17, 301447),
                new Student(4, "Tonu", "Polster", 17, 301081),
                new Student(5, "Axel", "Schweiß", 13, 311014),
                new Student(3, "Sebastian", "Short", 15, 301052),
                new Student(9, "Susi", "Spreiz", 14, 311429),
                new Student(0, "Klausi", "Wartmann", 19, 301447),
                new Student(7, "Donna", "Wetter", 15, 311418),
                new Student(10, "Anna", "Will", 19, 311429)

             };
        }


    }
}
