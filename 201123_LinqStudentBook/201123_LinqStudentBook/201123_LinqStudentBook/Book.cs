﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201123_LinqStudentBook
{
    public class Book
    {
        public Book(string title, int pages, double price)
        {
            Title = title;
            Pages = pages;
            Price = price;
        }

        public string Title { get; set; }
        public int Pages { get; set; }
        public double Price { get; set; }
        public static List<Book> GetBooks()
        {
            return new List<Book>{
                new Book("Faust",94,12.99),
                new Book("How to train a Dragon",4654564,14.87),
                new Book("SEW-Foliensatz",516545,200.95),
                new Book("Monopoly-Anleitung",20,15.99),
                new Book("Bibel",1250,5.99),
                new Book("Wörterbuch",958,15.99),
                new Book("Speisekarte - Pizzeria Al Capone",8,1.99)


            };
        }

    }
}
