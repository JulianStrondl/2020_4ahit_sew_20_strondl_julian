﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace _201123_LinqStudentBook
{
   public  class XMLLogger : ILogger
    {
        public string path { get ; set; }
        XDocument doc;

        public void Log(string message, string result)
        {
            
            XElement msg =
                new XElement("Log",
                    new XElement("Message", message),
                    new XElement("result", result)
                    );
            doc.Element("Logs").Add(msg);
            doc.Save(path);
            
        }
        public XMLLogger(string path)
        {
            this.path = path;
            if (File.Exists(path))
            {
                doc = XDocument.Load(path);
            }
            else
            {
                doc = XDocument.Parse(@"<Logs></Logs>");
            }
        }
    }
}
