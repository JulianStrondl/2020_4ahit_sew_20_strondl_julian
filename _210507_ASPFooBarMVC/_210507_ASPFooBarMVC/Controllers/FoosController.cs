using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using _210507_ASPFooBarMVC.Models;

namespace _210507_ASPFooBarMVC.Controllers
{
    public class FoosController : Controller
    {
        private readonly FooBarContext _context;

        public FoosController(FooBarContext context)
        {
            _context = context;
        }

        // GET: Foos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Foos.ToListAsync());
        }
        
        

        // GET: Foos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var foo = await _context.Foos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (foo == null)
            {
                return NotFound();
            }

            return View(foo);
        }

        // GET: Foos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Foos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Height,CreatedDate")] Foo foo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(foo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(foo);
        }

        // GET: Foos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var foo = await _context.Foos.FindAsync(id);
            if (foo == null)
            {
                return NotFound();
            }
            return View(foo);
        }

        // POST: Foos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Height,CreatedDate")] Foo foo)
        {
            if (id != foo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(foo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FooExists(foo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(foo);
        }

        // GET: Foos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var foo = await _context.Foos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (foo == null)
            {
                return NotFound();
            }

            return View(foo);
        }

        // POST: Foos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var foo = await _context.Foos.FindAsync(id);
            _context.Foos.Remove(foo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FooExists(int id)
        {
            return _context.Foos.Any(e => e.Id == id);
        }
    }
}
