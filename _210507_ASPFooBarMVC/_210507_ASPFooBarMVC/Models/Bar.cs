﻿using System.ComponentModel.DataAnnotations;

namespace _210507_ASPFooBarMVC.Models
{
    
    public class Bar
    {
       
        public int Id { get; set; }
        
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }
        [Required]
        public bool IsBar { get; set; }
        
        [RegularExpression(@"^[A-Z]+[a-zA-Z0-9""'\s-]*$")]
        [StringLength(5)]
        [Required]
        public string Rating { get; set; }
        
    }
}