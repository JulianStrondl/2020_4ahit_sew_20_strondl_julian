﻿using System;

namespace _210507_ASPFooBarMVC.Models
{
   
    public class Foo
    {
       
        public int Id { get; set; }
        
        public double Height { get; set; }
       
        public DateTime CreatedDate { get; set; }
    }
}