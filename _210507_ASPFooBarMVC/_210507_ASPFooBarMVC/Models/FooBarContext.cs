﻿using Microsoft.EntityFrameworkCore;

namespace _210507_ASPFooBarMVC.Models
{
    public class FooBarContext:DbContext
    {
        public DbSet<Foo> Foos { get; set; }
        public DbSet<Bar> Bars { get; set; }

        public FooBarContext()
        {
            
        }
        public FooBarContext(DbContextOptions<FooBarContext> options):
            base(options){}
    }
}