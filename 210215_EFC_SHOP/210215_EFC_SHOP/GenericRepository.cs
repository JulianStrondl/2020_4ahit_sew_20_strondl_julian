﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace _210215_EFC_SHOP
{
    public abstract class ARepository<T, TId> : IDisposable where T: class
    {
        protected ShopContext Context { get; }
        protected abstract DbSet<T> Table { get; }

        public void Dispose()
        {
            Context.Dispose();
        }
        public ARepository(ShopContext context)
        {
            this.Context = context;
        }
        public ARepository()
        {
            this.Context = new ShopContext();
        }
        public List<T> GetAll() => Table.ToList();
        public T GetOne(TId id) => Table.Find(id);
        public int Add(T entity) {
            Table.Add(entity);
            return Context.SaveChanges();
        }
        public int Delete(T entity)
        {
            Table.Remove(entity);
            return Context.SaveChanges();
        }
        public int Update() => Context.SaveChanges();
    }
}
