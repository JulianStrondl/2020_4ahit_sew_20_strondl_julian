﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210215_EFC_SHOP
{
    [Table("SecurityEmployees")]
    public class SecurityEmployee :Employee
    {
        public int WorkedHours { get; set; }
    }
}
