﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _210215_EFC_SHOP
{
    public class CustomerRepository:ARepository<Customer,int>
    {
        ShopContext contex = new ShopContext();

        protected override DbSet<Customer> Table => Context.Customers;

        public List<Customer> GetCustomersbyLastname(string lastname)
        {
            return contex.Customers.Where(e => e.LastName == lastname).ToList();
        }
        public List<Customer> GetCustomersOrderbyName()
        {
            return contex.Customers.OrderBy(c => c.LastName).ToList();
        }
        public List<Customer> GetCustomersbyPhoneNumber(string number)
        {
            return contex.Customers.Where(e => e.PhoneNumber == number).ToList();
        }

    }
}
