﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace _210215_EFC_SHOP
{
    public class GoodRepository : ARepository<Good, int>
    {
       public  ShopContext contex = new ShopContext();
        protected override DbSet<Good> Table => contex.Goods;
        public List<Good> GetGoodsByCategory(ECategory category)
        {
            return contex.Goods.Where(g => g.Category == category).ToList();
        }
        public List<Good> GetGoodsbyMaxPrice(double price)
        {
            return contex.Goods.Where(g => g.Price > price).ToList();
        }
        public List<Good> GetGoodsbyName(string name)
        {
            return contex.Goods.Where(g => g.Name == name).ToList();
        }

    }
}
