﻿using System;
using System.Linq;

namespace _210215_EFC_SHOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Shop Entity Framework Example");
            TestBasic();
        }
        static void TestBasic()
        {
            using (var context = new ShopContext())
            {
                var C01 = new Customer() { Firstname = "Schorsch", LastName = "Mulka", PhoneNumber = "2848481295" };
                var emp01 = new Employee() { Firstname = "Schorsch", LastName = "Mulka", Salary = 1000 };
                var g01 = new Good() { Name = "Banan", Price = 5, Category = ECategory.FOOD };
                
                var P01 = new PersonalInformation() { BIC = "BAWAAA", Birthdate = new DateTime(2000, 5, 24), IBAN = "AT66666666666", SocialSecurityNumber = "848941651" , Employee = emp01, Id= emp01.Id};

                var Sm1= new Salesman() { Firstname = "Salesman1", LastName = "Mulka", Salary = 1000,   ServedCustomers=25};
                var b01 = new Bill() { ChargedAmount = 25, ResponsibleSalesman = Sm1};
                var Sc1= new SecurityEmployee() { Firstname = "SecurityEmployee1", LastName = "Mulka", Salary = 1000,  WorkedHours = 32 };
                var Ac1= new AccountingEmployee() { Firstname = "AccountingEmployee1", LastName = "Mulka", Salary = 1000,  Education = "HAK"};

                
                context.Customers.Add(C01);
                context.Employees.Add(emp01);
                context.Goods.Add(g01);
                context.Bills.Add(b01);
                context.PersonalInformations.Add(P01);
                context.AccountingEmployees.Add(Ac1);
                context.SecurityEmployees.Add(Sc1);
                context.Salesmen.Add(Sm1);
                
                context.SaveChanges();
                context.Entry(Sm1).Property("HiredDate").CurrentValue=DateTime.Now;
                context.SaveChanges();
                Console.WriteLine(context.Customers.First());
                Console.WriteLine(context.Employees.First());
                Console.WriteLine(context.Goods.First());
                Console.WriteLine(context.Bills.First());
                Console.WriteLine(context.AccountingEmployees.First());
                Console.WriteLine(context.SecurityEmployees.First());
                Console.WriteLine(context.PersonalInformations.First());
                Console.WriteLine(context.Salesmen.First());

            }
        }

    }
}
