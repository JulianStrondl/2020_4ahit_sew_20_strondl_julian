﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _210215_EFC_SHOP
{
    public class EmployeeRepository:ARepository<Employee,int>
    {
       public  ShopContext contex = new ShopContext();

        protected override DbSet<Employee> Table => contex.Employees;

        public int GetAmountofEmployees()
        {
            return contex.Employees.Count();
        }
        public double GetBestSalary()
        {
            return contex.Employees.Max(e => e.Salary);
        }
        public void PrintEmpsLZY()
        {
            var emp = contex.Employees;
            foreach (var item in emp)
            {
                Console.WriteLine(item.LastName);
                Console.WriteLine(item.persInfo.SocialSecurityNumber);
            }
        }
        public void PrintEmpsEGR()
        {
            var emp = contex.Employees.Include(e => e.persInfo);
            foreach (var item in emp)
            {
                Console.WriteLine(item.LastName);
                Console.WriteLine(item.persInfo.SocialSecurityNumber);
            }
        }

    }
}
