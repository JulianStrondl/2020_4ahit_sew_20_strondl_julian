﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace _210215_EFC_SHOP
{
    [Table("Salesmen")]
    public class Salesman :Employee
    {
        public int ServedCustomers { get; set; }
    }
}
