﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_SHOP
{
    public class Customer
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public override string ToString()
        {
            return Firstname + " " + LastName;
        }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
