﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_SHOP
{
    public class Bill
    {
        public int Id { get; set; }
        public double ChargedAmount { get; set; }

        
        public Salesman ResponsibleSalesman { get; set; }
        
        public Customer Customer { get; set; }
        
        public override string ToString()
        {
            return Id + " " + ChargedAmount;
        }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
