﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_SHOP
{
    public class PersonalInformation
    {
        public int Id{get;set;}
        public string SocialSecurityNumber { get; set; }
        public DateTime Birthdate { get; set; }

        public string IBAN { get; set; }
        public string BIC { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
        public override string ToString()
        {
            return SocialSecurityNumber;
        }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
