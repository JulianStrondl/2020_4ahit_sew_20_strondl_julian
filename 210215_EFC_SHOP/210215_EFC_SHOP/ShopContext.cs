﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace _210215_EFC_SHOP
{
    public class ShopContext:DbContext
    {
        public static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); } );
        public ShopContext()
        {

        }

        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging().UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;
                                            Database=ShopDB;Trusted_Connection=True;");
        }
        protected override void OnModelCreating(ModelBuilder builder) 
        {
            builder.Entity<GoodBill>().HasKey(sc => new { sc.GoodId, sc.BillId });
            //Shadow Property
            builder.Entity<Employee>().Property<DateTime>("HiredDate");
        }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Good> Goods { get; set; }
        public DbSet<PersonalInformation> PersonalInformations { get; set; }
        public DbSet<AccountingEmployee> AccountingEmployees { get; set; }
        public DbSet<SecurityEmployee> SecurityEmployees { get; set; }
        public DbSet<Salesman> Salesmen { get; set; }
    }
}
