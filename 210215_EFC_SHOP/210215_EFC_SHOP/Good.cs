﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_SHOP
{
    public enum ECategory { FOOD, ELECTRONICS, HARDWARE, BOOKS, CLOTHES  }

    public class Good
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        
        public ECategory Category { get; set; }
        
        public override string ToString()
        {
            return  Name;
        }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
