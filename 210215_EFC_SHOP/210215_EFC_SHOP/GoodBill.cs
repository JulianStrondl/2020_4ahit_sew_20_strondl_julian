﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace _210215_EFC_SHOP
{
    public class GoodBill
    {
        public int GoodId { get; set; }
        public IList<Good> Good { get; set; }

        public int BillId { get; set; }
        public IList<Bill> Bill{get;set;}
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
