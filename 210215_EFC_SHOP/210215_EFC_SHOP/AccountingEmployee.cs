﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace _210215_EFC_SHOP
{
    [Table("AccountingEmployees")]
    public class AccountingEmployee :Employee
    {
        public string Education { get; set; }
    }
}
