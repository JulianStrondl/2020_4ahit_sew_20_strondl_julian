using Microsoft.VisualStudio.TestTools.UnitTesting;
using _210215_EFC_SHOP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopTest
{
    [TestClass]
    public class UnitTest1
    {
        GoodRepository gRepo = new GoodRepository();
        [TestMethod]
        public void GetGoodsbyCategory()
        {
            Good aFood = gRepo.contex.Goods.ToList()[0];
            Assert.AreEqual(aFood, gRepo.GetGoodsByCategory(ECategory.FOOD)[0]);

        }
        [TestMethod]
        public void GetGoodsbyPrice()
        {
            double price = 2.99;

            Assert.IsTrue(price < gRepo.GetGoodsbyMaxPrice(price)[0].Price);

        }
        [TestMethod]
        public void GetGoodsbyName()
        {
            string name = "Banan";
            Assert.AreEqual(name, gRepo.GetGoodsbyName(name)[0].Name);
        }
        CustomerRepository cRepo = new CustomerRepository();
        [TestMethod]
        public void GetCustomerbyLastName()
        {
            Customer c = new Customer() { Firstname = "Hauns", LastName = "Fraunz", PhoneNumber = "06643928611" };
            cRepo.Add(c);
            Assert.AreEqual(c.LastName, cRepo.GetCustomersbyLastname(c.LastName)[0].LastName);
        }
        [TestMethod]
        public void GetCustomerbyPhoneNumber()
        {
            Assert.AreEqual("06643928611", cRepo.GetCustomersbyPhoneNumber("06643928611")[0].PhoneNumber);

        }
        EmployeeRepository eRepo = new EmployeeRepository();
        [TestMethod]
        public void getEmployeeCount()
        {
            int cnt = eRepo.contex.Employees.Count();
            Assert.AreEqual(eRepo.GetAmountofEmployees(), cnt);
        }
        [TestMethod]
        public void GetBestSalary()
        {
            
            
            Assert.AreEqual(eRepo.GetBestSalary(),1000);
        }
        
        [TestMethod]
        [ExpectedException (typeof(NullReferenceException))]
        public void TestEagerLoading()
        {
            eRepo.PrintEmpsLZY();
        }
    }
}
