﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200910_RecapitulationBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(Greatestdivider(16,24));
            //Fibonacci();
            Console.WriteLine(Factory(10)); 
        }
        private static long Factory(int f)
        {
            ;
            if (f==1)
            {
                
                return 1;
            }
            else
            {
                long res= Factory(f - 1) * f;
                return res;
            }
            
            

        }
        
        private static void Fibonacci()
        {
            Console.WriteLine(1);
            Fibonacci(0, 1);
        }
        private static void Fibonacci(int a, int b)
        {
            Console.WriteLine(a+b);
            a = a + b;
            System.Threading.Thread.Sleep(100);
            Fibonacci(b,a);
        }
        private static int Greatestdivider(int a, int b)
        {
            if (a==0)
                return b;

            if (b == 0)
                return a;

            if (a>b)
                return Greatestdivider(a - b,b);
            else 
               return Greatestdivider(a, b - a);
            

        }
    }
}
