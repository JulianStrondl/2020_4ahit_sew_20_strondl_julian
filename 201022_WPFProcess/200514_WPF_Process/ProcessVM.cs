﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200514_WPF_Process
{
    class ProcessVM:ANotifyPropertyChanged
    {
        public Process process;
        public ProcessVM()
        {
            process = new Process();
        }

        public ProcessVM(string description, int id, int importance, int progressPercent, bool sheduled, EState state, bool threadSafe):this()
        {
            Description = description;
            Id = id;
            Importance = importance;
            ProgressPercent = progressPercent;
            Sheduled = sheduled;
            State = state;
            ThreadSafe = threadSafe;
        }

        public string Description
        {
            get
            {
                return process.Description;
            }
            set
            {
                process.Description = value;
                OnPropertyChanged();
            }
        }
        public int Id
        {
            get
            {
                return process.Id;
            }
            set
            {
                process.Id = value;
                OnPropertyChanged();
            }
        }
        public int Importance
        {
            get
            {
                return process.Importance;
            }
            set
            {
                process.Importance = value;
                OnPropertyChanged();
            }
        }
        public int ProgressPercent
        {
            get
            {
                return process.ProgressPercent;
            }
            set
            {
                process.ProgressPercent = value;
                OnPropertyChanged();
            }
        }
        public bool Sheduled
        {
            get
            {
                return process.Sheduled;
            }
            set
            {
                process.Sheduled = value;
                OnPropertyChanged();
            }
        }
        public EState State
        {
            get
            {
                return process.State;
            }
            set
            {
                process.State = value;
                OnPropertyChanged();
            }
        }
        public bool ThreadSafe
        {
            get
            {
                return process.ThreadSafe;
            }
            set
            {
                process.ThreadSafe = value;
                OnPropertyChanged();
            }
        }
        public Process GetProcess()
        {
            return process;
        }


    }
}
