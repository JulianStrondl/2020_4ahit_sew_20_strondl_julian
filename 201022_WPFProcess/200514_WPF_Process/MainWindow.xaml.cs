﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _200514_WPF_Process
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var VM = DataContext as ProcessesVM;
            VM.AddToList(Process.Read());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var VM = DataContext as ProcessesVM;
            VM.SaveInCSV();
        }
        
    }
}
