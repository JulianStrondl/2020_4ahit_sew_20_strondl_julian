﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200514_WPF_Process
{
    enum EState { RUNNING,BLOCKED,READY}
    class Process
    {
        public Process() { }
        private int id;
        //Value [1-10]
        private int importance;
        private bool sheduled;
        private string description;
        private bool threadSafe;
        private int progressPercent;
        private EState state;
        public int Id { get => id; set => id = value; }
        public int Importance { get => importance; set => importance = value; }
        public bool Sheduled { get => sheduled; set => sheduled = value; }
        public bool ThreadSafe { get => threadSafe; set => threadSafe = value; }
        public string Description { get => description; set => description = value; }
        public int ProgressPercent { get => progressPercent; set => progressPercent = value; }
        public EState State { get => state; set => state = value; }
        public Process(int id, int importance, bool sheduled,
        bool threadsafe, string desc, int progress, EState state)
        {
            this.id = id;
            this.importance = importance;
            this.sheduled = sheduled;
            this.threadSafe = threadsafe;
            this.description = desc;
            this.progressPercent = progress;
            this.state = state;
        }
        public string ToCSv()
        {
            return $"{Id};{Importance};{Sheduled};{ThreadSafe};{Description};{ProgressPercent};{State}";
        }
        public static List<Process> Read()
        {
            List<Process> processes = new List<Process>();
            if (File.Exists("process.csv"))
            {
                StreamReader sr = new StreamReader("process.csv",true);

                string line;
                string[] words;
                using (sr)
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = line.Split(';');
                        processes.Add(new Process(Convert.ToInt32(words[0]), Convert.ToInt32(words[1]), Convert.ToBoolean(words[2]), Convert.ToBoolean(words[3]), words[4], Convert.ToInt32(words[5]), (EState)Enum.Parse(typeof(EState), words[6])));
                    }
                }
            }
            
            return processes;
        }
    }
}
