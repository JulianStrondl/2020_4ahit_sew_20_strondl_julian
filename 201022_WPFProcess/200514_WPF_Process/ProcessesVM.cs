﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200514_WPF_Process
{
    class ProcessesVM:ANotifyPropertyChanged
    {
        public ObservableCollection<ProcessVM> Processes { get; set; }
        ProcessVM currentProcess;
        public ProcessesVM()
        {
            currentProcess = new ProcessVM();
            Processes = new ObservableCollection<ProcessVM>();
            selectedItem = new ProcessVM();
        }
        public ProcessVM CurrentProcess
        {
            get
            {
                return currentProcess;
            }
            set
            {
                currentProcess = value;
                OnPropertyChanged();
            }

        }

        ProcessVM selectedItem;
        public ProcessVM SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                OnPropertyChanged();
            }

        }
        public RelayCommand AddCommand => new RelayCommand(
            () =>
            {
                Processes.Add(CurrentProcess);
                SelectedItem = null;
                CurrentProcess = null;
                CurrentProcess = new ProcessVM();
            }
            );
        
        public RelayCommand ClearCommand => new RelayCommand(() => SelectedItem = null);

        public RelayCommand DeleteCommand => new RelayCommand(() => Processes.Remove(SelectedItem));
        public void AddToList(List<Process> ls)
        {
            List < ProcessVM > lsx= ConvToProcessVM(ls);
            foreach (ProcessVM item in lsx)
            {
                Processes.Add(item);
            }
        }
        public List<ProcessVM> ConvToProcessVM(List<Process> ls)
        {
            List<ProcessVM> lsx = new List<ProcessVM>();
            foreach (Process item in ls)
            {
                lsx.Add(new ProcessVM(item.Description,item.Id,item.Importance,item.ProgressPercent,item.Sheduled,item.State,item.ThreadSafe));
            }
            return lsx;
        }
        public void SaveInCSV()
        {
            StreamWriter sw = new StreamWriter("process.csv");
            using (sw)
            {
                foreach (ProcessVM item in Processes)
                {
                    sw.WriteLine(item.process.ToCSv());
                }
            }
            
        }
    }

}
