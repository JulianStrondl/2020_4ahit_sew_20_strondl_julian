﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_Process
{
    class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        readonly Func<bool> _canExecute;
        readonly Action _execute;

        public RelayCommand(Action execute) : this(execute, null)
        {

        }
        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute==null)
            {
                throw new ArgumentNullException("execute");
            }
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true:_canExecute();
        }

        public void Execute(object parameter)
        {
            _execute();
        }
    }
}
