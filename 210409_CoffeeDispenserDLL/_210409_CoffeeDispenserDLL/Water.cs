﻿namespace _210409_CoffeeDispenserDLL
{
    public class Water:ABeverage
    { 
        public new static double Price = 0.5;
        public Water(int milk, int soy, int sugar) : base(milk, soy, sugar)
        {
            
        }
        public override string ToString()
        {
            return
                $"{this.GetType().ToString().Split('.')[1]},  Price: {Price}, Milk: {MilkAmount}, Soy: {SoyAmount}, Sugar:{SugarAmount}";
        }
    }
}