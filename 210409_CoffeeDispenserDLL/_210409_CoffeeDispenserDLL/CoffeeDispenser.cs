﻿



using System.ComponentModel;

namespace _210409_CoffeeDispenserDLL
{
    
    public class CoffeeDispenser:ICoffeeDispenser
    {
        public CoffeeDispenser()
        {
            Earnings = 0;
            MilkAmount = 100;
            SugarAmount = 100;
            SoyMilkAmount = 100;
            WaterAmount = 100;
        }


        public double Earnings { get; private set; }
        public int MilkAmount { get;private set; }
        public int SugarAmount { get; private set;}
        public int SoyMilkAmount { get;private set; }
        public int WaterAmount { get;private set; }
        
        

        public ABeverage Order(EBeverage b, ESugar s, EMilk m)
        {
            AmountClass a = AmountClass.GetAdditivesValues(s, m);
            CheckIfEmpty(a);
            switch (b)
            {
                case EBeverage.TEA:
                    Earnings += Tea.Price;
                    return new Tea(a.Milk, a.Soy, a.Sugar);
                    
                case EBeverage.CACAO:
                    Earnings += Cacao.Price;
                    return new Cacao(a.Milk, a.Soy, a.Sugar);
                    
                case EBeverage.COFFEE:
                    Earnings += Coffee.Price;
                    return new Coffee(a.Milk, a.Soy, a.Sugar);
                    
                case EBeverage.JAEGERMEISTER:
                    Earnings += Jaegermeister.Price;
                    return new Jaegermeister(a.Milk, a.Soy, a.Sugar);
                    
                case EBeverage.WATER:
                    Earnings += Water.Price;
                    return new Jaegermeister(a.Milk, a.Soy, a.Sugar); 
                    
                default:
                    throw new InvalidEnumArgumentException();
                    
            }
        }

        public string Refill()
        {
            MilkAmount = 100;
            SugarAmount = 100;
            SoyMilkAmount = 100;
            WaterAmount = 100;
            
            return "Sucessfully Refilled";
        }

        private void CheckIfEmpty(AmountClass a)
        {
            if (MilkAmount- a.Milk < 0)
            {
                throw new OutOfSuppliesException("Out of Milk");
            }

            if (SugarAmount - a.Sugar <0 )
            {
                throw new OutOfSuppliesException("Out of Sugar");
            }
            if (SoyMilkAmount - a.Soy <0 )
            {
                throw new OutOfSuppliesException("Out of Soy");
            }
        }

        private class AmountClass
        {
            public int Milk { get; set; }
            public int Soy { get; set; }  
            public int Sugar { get; set; }

            public static AmountClass GetAdditivesValues(ESugar s, EMilk m)
            {
                AmountClass a = new AmountClass();
                a.Sugar = AmountClass.AmntSugar(s);
                if (m == EMilk.SOJA)
                {
                    a.Soy = 1;
                }
                else
                {
                    a.Milk = AmntMilk(m);
                }

                return a;

            }
            private static int AmntSugar(ESugar s){
                if (s == ESugar.NONE)
                {
                    return 0;
                }
                else if (s==ESugar.SUGAR)
                {
                    return 1;
                }
                else if(s==ESugar.SUGAR_TWICE)
                {
                    return 2;
                }
                else
                {
                    throw new InvalidEnumArgumentException();
                }
            }
            private static int AmntMilk(EMilk s){
                if (s == EMilk.NONE)
                {
                    return 0;
                }
                else if (s==EMilk.MILK)
                {
                    return 1;
                }
                else
                {
                    throw new InvalidEnumArgumentException();
                }
            }
        }
        
    }
    
}