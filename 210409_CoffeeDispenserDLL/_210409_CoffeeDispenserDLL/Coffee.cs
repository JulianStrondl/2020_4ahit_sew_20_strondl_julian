﻿namespace _210409_CoffeeDispenserDLL
{
    public class Coffee:ABeverage
    {
        public new static double Price = 1.75;
        public Coffee(int milk, int soy, int sugar) : base(milk, soy, sugar)
        {
        }
        public override string ToString()
        {
            return
                $"{this.GetType().ToString().Split('.')[1]},  Price: {Price}, Milk: {MilkAmount}, Soy: {SoyAmount}, Sugar:{SugarAmount}";
        }
        
    }
}