﻿namespace _210409_CoffeeDispenserDLL
{
    public class Cacao:ABeverage
    {
        public new static double Price = 1.5;
        public Cacao(int milk, int soy, int sugar) : base(milk, soy, sugar)
        {
        }
        public override string ToString()
        {
            return
                $"{this.GetType().ToString().Split('.')[1]},  Price: {Price}, Milk: {MilkAmount}, Soy: {SoyAmount}, Sugar:{SugarAmount}";
        }
    }
}