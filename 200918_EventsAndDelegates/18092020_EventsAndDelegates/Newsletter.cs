﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace _18092020_EventsAndDelegates
{
    class Newsletter
    {
       // List<Subscriber> ls = new List<Subscriber>();
        delegate void PublishDelegate(string title);
        event PublishDelegate publish;
        public void Subscribe(Subscriber s)
        {
            publish += s.Receive;
        }
        public void Publish(string title)
        {
            publish(title);
        }
    }
}
