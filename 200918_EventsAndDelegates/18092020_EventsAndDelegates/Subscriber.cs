﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18092020_EventsAndDelegates
{
    class Subscriber
    {
        public  string Name { get; set; }
        public Subscriber(string name)
        {
            Name = name;
        }
        public void Receive(string title)
        {
            Console.WriteLine(Name+" has received "+title);
        }
    }
}
