﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18092020_EventsAndDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            Newsletter n = new Newsletter();
            Subscriber a = new Subscriber("Hans");
            Subscriber c = new Subscriber("Franz");
            Subscriber d = new Subscriber("Max");
            Subscriber e = new Subscriber("Moritz");
            Subscriber f = new Subscriber("Horst");
            n.Subscribe(a);
            n.Subscribe(c);
            n.Subscribe(d);
            n.Subscribe(e);         
            n.Subscribe(f);
            n.Publish("Hamster frisst Katze");



        }
    }
}
