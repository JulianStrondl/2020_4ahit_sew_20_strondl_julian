﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace OpenHouse
{

    class Tour
    {
        static SemaphoreSlim semPresentation = new SemaphoreSlim(3);
        static SemaphoreSlim semRoom1 = new SemaphoreSlim(2);
        static SemaphoreSlim semRoom23 = new SemaphoreSlim(6);
        static int groups=15;
        public static void StartTour()
        {
            Thread[] threads = new Thread[groups];
            for (int i = 0; i < groups; i++)
            {
                threads[i] = new Thread(GetInfo);
                threads[i].Name = "Gruppe " + i;
            }
            
            Parallel.ForEach(threads, t => t.Start());
        }
        static void GetInfo()
        {
            Presentation();
            Room1();
            Room2();
            Room3();
        }
        static void Presentation()
        {
            semPresentation.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("{0} hört sich die Präsentation an" , Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0} ist fertig mit der Präsentation", Thread.CurrentThread.Name);
            semPresentation.Release();

        }
        static void Room1()
        {
            semRoom1.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("{0} ist im Raum 1", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0} verlässt Raum 1", Thread.CurrentThread.Name);
            semRoom1.Release();
        }
        static void Room2()
        {
            semRoom23.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("{0} ist im Raum 2", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0} verlässt Raum 2", Thread.CurrentThread.Name);
            semRoom23.Release();
        }
        static void Room3()
        {
            semRoom23.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("{0} ist im Raum 3", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0} verlässt Raum 3", Thread.CurrentThread.Name);
            semRoom23.Release();
        }

    }
    

}
