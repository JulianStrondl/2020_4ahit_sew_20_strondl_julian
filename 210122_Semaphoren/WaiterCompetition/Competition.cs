﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WaiterCompetition
{
    class Competition
    {
        static SemaphoreSlim semMixer = new SemaphoreSlim(1);
        static SemaphoreSlim semWorkplaces= new SemaphoreSlim(3);
        static int waiter = 10;
        public static void StartCompetion()
        {
            Thread[] threads = new Thread[waiter];
            for (int i = 0; i < waiter; i++)
            {
                threads[i] = new Thread(PrepareCocktail);
                threads[i].Name = "Kellner " + i;
            }
            Console.WriteLine("Besprechung");
            Thread.Sleep(1000);
            Parallel.ForEach(threads, t => t.Start());
            
        }
        public static void PrepareCocktail()
        {
            semWorkplaces.Wait();
            Console.WriteLine(" {0} betritt die Bar", Thread.CurrentThread.Name);
            MixCocktail();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" {0} verlässt die Bar", Thread.CurrentThread.Name);
            semWorkplaces.Release();
        }
        public static void MixCocktail()
        {
            semMixer.Wait();
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("--- {0} ist hart am Mixen", Thread.CurrentThread.Name);
            Thread.Sleep(1000);
            Console.WriteLine("--- {0} ist fertig mit Mixen", Thread.CurrentThread.Name);
            semMixer.Release();
        }
    }
}
