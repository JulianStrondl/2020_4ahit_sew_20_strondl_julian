﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace McDonaldsSemaphore
{
    class Program
    {
        static int amount = 50;
        public static SemaphoreSlim semGate = new SemaphoreSlim(3);
        public static SemaphoreSlim semCustomer = new SemaphoreSlim(0);
        public static SemaphoreSlim semCounterr = new SemaphoreSlim(0);
        static void Main(string[] args)
        {
            Customer[] bus;
            Thread[] busThread;
            InitCustomerThread(out bus, out busThread, 50);
            StartCustomerThreads(busThread);

            char a = 'A';
            for (int i = 0; i < 3; i++)
            {
                string gateName = "Gata " + a + a + a;
                Thread counter1 = StartMcDCounter(gateName);
                a++;
            }

            //Start Customer Threads

        }
        static void InitCustomerThread(out Customer[] bus, out Thread[] busthread, int amount)
        {
            bus = new Customer[amount];
            busthread = new Thread[amount];
            for (int i = 0; i < amount; i++)
            {
                bus[i] = new Customer("Customer " + i);
                busthread[i] = new Thread(bus[i].Order);
                busthread[i].Name = "Customer " + i;
            }

        }
        static void StartCustomerThreads(Thread[] busThreads)
        {
            for (int i = 0; i < busThreads.Length && busThreads[i]!= null; i++)
            {
                busThreads[i].Start();
            }
        }
        public static Thread StartMcDCounter(string name)
        {
            Thread gate1 = new Thread(McDonalds.TakeOrder);
            gate1.Name = name;
            gate1.Start();
            return gate1;
        }
    }
}
