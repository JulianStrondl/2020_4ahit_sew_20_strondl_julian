﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace McDonaldsSemaphore
{
    class McDonalds
    {
        public static void TakeOrder()
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"--- {Thread.CurrentThread.Name} FREI ---");
                Program.semCustomer.Wait();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"--- {Thread.CurrentThread.Name} nimmt Bestellung entegegen ---");
                Thread.Sleep(1000);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"--- {Thread.CurrentThread.Name} Gibt bestellung aus ---");
                Program.semCounterr.Release();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"--- {Thread.CurrentThread.Name} wieder FREI ---");

            }
        }
    }
}
