﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace McDonaldsSemaphore
{
    class Customer
    {
        public string name;
        public Customer(string n)
        {
            name = n;
        }
        public void Order()
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"{Thread.CurrentThread.Name} stellt sich an ");
            Program.semGate.Wait();
            Program.semCustomer.Release();
            Program.semCounterr.Wait();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"  {Thread.CurrentThread.Name} Bestellung aufgegeben");
            Thread.Sleep(2000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"  {Thread.CurrentThread.Name} Bestellung erhalten");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($" {Thread.CurrentThread.Name} geht das Menü essen");

            Program.semGate.Release();

        }
    }
}
