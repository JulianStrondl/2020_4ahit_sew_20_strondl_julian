﻿using System;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Threading;
using _210409_CoffeeDispenserDLL;

namespace CoffeeDispenserConsole
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            EBeverage bev;
            EMilk milk;
            ESugar sug;
            int a;
            bool b;
            ICoffeeDispenser dispenser = new CoffeeDispenser();
            Console.WriteLine("Welcome to the CoffeeDispenser \n What do you want to order");
            while (true)
            {
                Console.Clear();
                Console.WriteLine("0 --> Water");
                Console.WriteLine("1 --> Jaegermeister");
                Console.WriteLine("2 --> Coffee");
                Console.WriteLine("3 --> Tea");
                Console.WriteLine("4 --> Cacao");
                Console.WriteLine("5 --> REFILL (Operator only)");
                //Check if Input is valid
                InputValidation(out a,0,5);

                //Refill 
                if (a==5)
                {
                    Console.WriteLine(dispenser.Refill());
                }
                //Order Drink
                else
                {
                    bev = IntToBeverage(a);
                    Console.WriteLine("How much milk do you want?");
                    Console.WriteLine("0 --> None ");
                    Console.WriteLine("1 --> Once");
                    Console.WriteLine("2 --> Soja");
                    InputValidation(out a,0,2);
                    milk = IntToMilk(a);
                    Console.WriteLine("How much Sugar do you want?");
                    Console.WriteLine("0 --> None ");
                    Console.WriteLine("1 --> Once");
                    Console.WriteLine("2 --> Twice");
                    InputValidation(out a,0,2);
                    sug = IntToSugar(a);

                    Console.WriteLine("Dispensing Drink ...");
                    Thread.Sleep(1000);
                    Console.WriteLine(dispenser.Order(bev,sug,milk));
                    Thread.Sleep(10000);
                    
    

                }
            }
        }

        private static EBeverage IntToBeverage(int a)
        {
            switch (a)
            {
                case 0:
                    return EBeverage.JAEGERMEISTER;
                case 1:
                    return EBeverage.WATER;
                case 2:
                    return EBeverage.COFFEE;
                case 3:
                    return EBeverage.TEA;
                case 4:
                    return EBeverage.CACAO;
                default:
                    throw new InvalidEnumArgumentException();
                
            }
        }

        private static EMilk IntToMilk(int a)
        {
            switch (a)
            {
                case 0:
                    return EMilk.NONE;
                case 1:
                    return EMilk.MILK;
                case 2:
                    return EMilk.SOJA;
                default:
                    throw new InvalidEnumArgumentException();
                
            }
        }
        private static ESugar IntToSugar(int a)
        {
            switch (a)
            {
                case 0:
                    return ESugar.NONE;
                case 1:
                    return ESugar.SUGAR;
                case 2:
                    return ESugar.SUGAR_TWICE;
                default:
                    throw new InvalidEnumArgumentException();
                
            }
        }

        private static void InputValidation(out int a, int lowerbounds, int higherbounds)
        {
            bool b;
            do
            {
                b = true;
                b=int.TryParse(Console.ReadLine(), out a);
                if (!(b && a >=lowerbounds && a<=higherbounds) )
                {
                    b = false;
                    Console.WriteLine("Wrong input try again");
                }
            } while (!b);
        }
    }
}