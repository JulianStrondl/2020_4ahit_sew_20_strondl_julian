﻿namespace _210409_CoffeeDispenserDLL
{
    public abstract class ABeverage
    {
        public ABeverage(int milk, int soy, int sugar)
        {
            this.MilkAmount = milk;
            this.SoyAmount = soy;
            this.SugarAmount= sugar;
        }

        public  static double Price;
        public int MilkAmount { get; set; }
        public int SoyAmount { get; set; }
        public int SugarAmount { get; set; }
        public override string ToString()
        {
            return
                $"{this.GetType().ToString().Split('.')[1]},  Price: {Price}, Milk: {MilkAmount}, Soy: {SoyAmount}, Sugar:{SugarAmount}";
        }
    }
}