﻿namespace _210409_CoffeeDispenserDLL
{
    public enum EMilk{NONE, MILK, SOJA}
    public enum ESugar {NONE, SUGAR, SUGAR_TWICE}
    public enum EBeverage {COFFEE, CACAO, TEA, JAEGERMEISTER,WATER}
    public interface ICoffeeDispenser
    {
        double Earnings { get; }
        int MilkAmount { get; }
        int SugarAmount { get; }
        int SoyMilkAmount { get; }
        
        ABeverage Order(EBeverage b, ESugar s, EMilk m);
        string Refill();
    }
}