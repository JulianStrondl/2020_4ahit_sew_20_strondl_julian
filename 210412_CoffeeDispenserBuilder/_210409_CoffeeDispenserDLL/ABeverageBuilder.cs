﻿namespace _210409_CoffeeDispenserDLL
{
    public abstract class ABeverageBuilder
    {
        protected ABeverage beverage;

        public ABeverage GetBeverage()
        {
            return  beverage;
        }

        public abstract void CreateNewBeverageProduct();
        public abstract void BuildSugar();
        public abstract void BuildMilk();
        public abstract void BuildSoy();
       
    }
}