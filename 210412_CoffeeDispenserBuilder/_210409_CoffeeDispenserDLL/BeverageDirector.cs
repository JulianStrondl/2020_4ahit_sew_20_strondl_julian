﻿namespace _210409_CoffeeDispenserDLL
{
    public class BeverageDirector
    {
        private ABeverageBuilder beverageBuilder;

        public void SetBeverageBuilder(ABeverageBuilder builder)
        {
            beverageBuilder = builder;
        }

        public ABeverage GetBeverage()
        {
            return beverageBuilder.GetBeverage();
        }

        public void ConstructBeverage()
        {
            beverageBuilder.CreateNewBeverageProduct();
            beverageBuilder.BuildMilk();
            beverageBuilder.BuildSoy();
            beverageBuilder.BuildSugar();
        }
    }
}