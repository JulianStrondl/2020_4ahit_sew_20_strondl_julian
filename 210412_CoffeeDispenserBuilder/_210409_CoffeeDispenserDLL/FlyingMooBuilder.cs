﻿namespace _210409_CoffeeDispenserDLL
{
    public class FlyingMooBuilder:ABeverageBuilder
    {
        public override void CreateNewBeverageProduct()
        {
            //Too lazy to make a standard contructor
            this.beverage = new Jaegermeister(0,0,0);
        }

        public override void BuildSugar()
        {
            this.beverage.SugarAmount = 0;
        }

        public override void BuildMilk()
        {
            this.beverage.MilkAmount = 2;
        }

        public override void BuildSoy()
        {
            this.beverage.SoyAmount = 0;
        }
    }
}