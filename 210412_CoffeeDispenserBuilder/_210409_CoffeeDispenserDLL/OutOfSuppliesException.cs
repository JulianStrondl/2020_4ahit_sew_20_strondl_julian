﻿using System;

namespace _210409_CoffeeDispenserDLL
{
    public class OutOfSuppliesException:Exception
    {
        public OutOfSuppliesException(string message):base(message){}
    }
}