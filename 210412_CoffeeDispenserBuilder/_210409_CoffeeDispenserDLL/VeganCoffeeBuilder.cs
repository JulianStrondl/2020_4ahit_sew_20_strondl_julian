﻿namespace _210409_CoffeeDispenserDLL
{
    public class VeganCoffeeBuilder:ABeverageBuilder
    {
        public override void CreateNewBeverageProduct()
        {
            //Too lazy to make a standard contructor
            this.beverage = new Coffee(0,0,0);
        }

        public override void BuildSugar()
        {
            this.beverage.SugarAmount = 0;
        }

        public override void BuildMilk()
        {
            this.beverage.SugarAmount = 0;
        }

        public override void BuildSoy()
        {
            this.beverage.SoyAmount = 2;
        }
    }
}