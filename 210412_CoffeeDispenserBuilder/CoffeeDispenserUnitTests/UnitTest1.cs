using System;
using _210409_CoffeeDispenserDLL;
using NUnit.Framework;

namespace CoffeeDispenserUnitTests
{
    public class Tests
    {
        
        [Test]
        public void TestBlackCoffeeBuilder()
        {
            BeverageDirector director = new BeverageDirector();
            ABeverageBuilder builder = new BlackCoffeeBuilder();
            
            director.SetBeverageBuilder(builder);
            director.ConstructBeverage();

            ABeverage bev = director.GetBeverage();
            
            Assert.IsTrue(bev.GetType()== typeof(Coffee) && bev.SugarAmount==0 && bev.MilkAmount==0 && bev.SoyAmount==0 );
        }
        [Test]
        public void TestVeganCoffeeBuilder()
        {
            BeverageDirector director = new BeverageDirector();
            ABeverageBuilder builder = new VeganCoffeeBuilder();
            
            director.SetBeverageBuilder(builder);
            director.ConstructBeverage();

            ABeverage bev = director.GetBeverage();
            
            Assert.IsTrue(bev.GetType()== typeof(Coffee) && bev.SugarAmount==0 && bev.MilkAmount==0 && bev.SoyAmount==2 );
        }
        [Test]
        public void TestFlyingMooBuilder()
        {
            BeverageDirector director = new BeverageDirector();
            ABeverageBuilder builder = new FlyingMooBuilder();
            
            director.SetBeverageBuilder(builder);
            director.ConstructBeverage();

            ABeverage bev = director.GetBeverage();
            
            
            Assert.IsTrue(bev.GetType()== typeof(Jaegermeister) && bev.SugarAmount==0 && bev.MilkAmount==2 && bev.SoyAmount==0 );
        }
    }
}